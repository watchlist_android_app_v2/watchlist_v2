package com.vrares.watchlist.moviedb.moviedetails.usecase;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.entities.social.FeedPost;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.moviedb.moviedetails.presentation.MovieDetailsContract;
import com.vrares.watchlist.moviedb.watch.presentation.WatchListContract;

import java.sql.Timestamp;
import java.util.Calendar;

import javax.inject.Singleton;

import static com.vrares.watchlist.core.Constants.COLLECTION;
import static com.vrares.watchlist.core.Constants.EMAIL;
import static com.vrares.watchlist.core.Constants.FEED;
import static com.vrares.watchlist.core.Constants.NAME;
import static com.vrares.watchlist.core.Constants.PICTURE;
import static com.vrares.watchlist.core.Constants.USERS;
import static com.vrares.watchlist.core.Constants.WATCHLIST;

@Singleton
public class AddToListUseCase {

    public void addMovieToList(MovieDetails movieDetails, String listType, MovieDetailsContract.Presenter callback) {
        MovieWatched movieWatched = null;
        if (listType.equals(COLLECTION)) {
            String timestamp = new Timestamp(System.currentTimeMillis()).toString();
            movieWatched = new MovieWatched(movieDetails, timestamp);
        }
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference(USERS);
        usersRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(WATCHLIST)
                .child(movieDetails.getId().toString())
                .removeValue();
        usersRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(listType)
                .child(movieDetails.getId().toString())
                .setValue(listType.equals(COLLECTION) ? movieWatched : movieDetails)
                .addOnCompleteListener(new SaveMovieToNewsFeedCompleteListener(movieDetails, callback, listType));
    }

    public void addMovieToList(MovieDetails movieDetails, String listType, WatchListContract.Presenter callback) {
        MovieWatched movieWatched = null;
        if (listType.equals(COLLECTION)) {
            String timestamp = new Timestamp(System.currentTimeMillis()).toString();
            movieWatched = new MovieWatched(movieDetails, timestamp);
        }
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference(USERS);
        usersRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(listType)
                .child(movieDetails.getId().toString())
                .setValue(listType.equals(COLLECTION) ? movieWatched : movieDetails)
                .addOnCompleteListener(new SaveMovieToNewsFeedCompleteListener(movieDetails, callback, listType));
    }

    private class SaveMovieToNewsFeedCompleteListener implements OnCompleteListener<Void> {

        private MovieDetails movieDetails;
        private WatchListContract.Presenter callback;
        private MovieDetailsContract.Presenter detailsCallback;
        private String listType;

        public SaveMovieToNewsFeedCompleteListener(MovieDetails movieDetails, WatchListContract.Presenter callback, String listType) {
            this.movieDetails = movieDetails;
            this.callback = callback;
            this.listType = listType;
        }

        public SaveMovieToNewsFeedCompleteListener(MovieDetails movieDetails, MovieDetailsContract.Presenter callback, String listType) {
            this.movieDetails = movieDetails;
            this.detailsCallback = callback;
            this.listType = listType;
        }

        @Override
        public void onComplete(@NonNull Task<Void> task) {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            final DatabaseReference feedReference = FirebaseDatabase.getInstance().getReference(FEED);
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = new User();
                    user.setId(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    user.setName(dataSnapshot.child(NAME).getValue().toString());
                    user.setEmail(dataSnapshot.child(EMAIL).getValue().toString());
                    user.setPicture(dataSnapshot.child(PICTURE).getValue().toString());

                    FeedPost feedPost = new FeedPost();
                    feedPost.movieDetails = movieDetails;
                    feedPost.user = user;
                    feedPost.listType = listType;
                    feedPost.timestamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
                    feedPost.id = feedReference.push().getKey();

                    feedReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .child(feedPost.id)
                            .setValue(feedPost)
                            .addOnCompleteListener(callback == null
                                    ? new DatabaseInsertionCompleteListener(detailsCallback, listType)
                                    : new DatabaseInsertionCompleteListener(movieDetails, callback, listType));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private class DatabaseInsertionCompleteListener implements OnCompleteListener<Void> {

        private MovieDetailsContract.Presenter detailsCallback;
        private WatchListContract.Presenter watchlistCallback;
        private String listType;
        private MovieDetails movieDetails;

        public DatabaseInsertionCompleteListener(MovieDetailsContract.Presenter detailsCallback, String listType) {
            this.detailsCallback = detailsCallback;
            this.listType = listType;
        }

        public DatabaseInsertionCompleteListener(MovieDetails movieDetails, WatchListContract.Presenter watchlistCallback, String listType) {
            this.watchlistCallback = watchlistCallback;
            this.listType = listType;
            this.movieDetails = movieDetails;
        }

        @Override
        public void onComplete(@NonNull Task<Void> task) {
            if (task.isSuccessful()) {
                if (watchlistCallback == null) {
                    detailsCallback.onMovieAddedToList(listType);
                } else {
                    watchlistCallback.onMovieAddedToList(movieDetails, listType);
                }
            }
        }
    }
}
