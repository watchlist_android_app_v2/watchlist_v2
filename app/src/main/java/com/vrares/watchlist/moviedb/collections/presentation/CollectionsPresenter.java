package com.vrares.watchlist.moviedb.collections.presentation;

import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.moviedb.collections.usecase.GetCollectionUseCase;

import java.util.ArrayList;

import javax.inject.Inject;

public class CollectionsPresenter implements CollectionsContract.Presenter {

    @Inject
    GetCollectionUseCase getCollectionUseCase;

    private CollectionsContract.View view;

    public void attach(CollectionsContract.View view) {
        this.view = view;
    }

    public void detach() {
        this.view = null;
    }

    public void getCollection() {
        getCollectionUseCase.getCollection(this);
    }

    @Override
    public void onCollectionReceived(ArrayList<MovieWatched> movieCollection) {
        if (view != null) {
            view.onCollectionReceived(movieCollection);
        }
    }

    @Override
    public void onEmptyCollection() {
        if (view != null) {
            view.onEmptyCollection();
        }
    }
}
