package com.vrares.watchlist.moviedb.watch.presentation;

import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.moviedb.moviedetails.usecase.AddToListUseCase;
import com.vrares.watchlist.moviedb.watch.usecase.GetWatchListUseCase;
import com.vrares.watchlist.moviedb.watch.usecase.RemoveFromListUseCase;

import java.util.ArrayList;
import javax.inject.Inject;

public class WatchListPresenter implements WatchListContract.Presenter {

    @Inject GetWatchListUseCase getWatchListUseCase;
    @Inject AddToListUseCase addToListUseCase;
    @Inject RemoveFromListUseCase removeFromListUseCase;

    private WatchListContract.View view;

    public void attach(WatchListContract.View view) {
        this.view = view;
    }

    public void detach() {
        this.view = null;
    }

    public void getLists() {
        getWatchListUseCase.getWatchList(this);
    }

    public void addMovieToList(MovieDetails movie, String favourites) {
        addToListUseCase.addMovieToList(movie, favourites, this);
    }

    public void removeFromList(MovieDetails movieDetails, String listType) {
        removeFromListUseCase.removeFromList(movieDetails, listType);
    }

    @Override
    public void onWatchListEmpty() {
        if (view != null) {
            view.onWatchListEmpty();
        }
    }

    @Override
    public void onWatchListReceived(ArrayList<MovieDetails> watchList, ArrayList<MovieDetails> favourites) {
        if (view != null) {
            view.onWatchListReceived(watchList, favourites);
        }
    }

    @Override
    public void onMovieAddedToList(MovieDetails movieDetails, String listType) {
        view.onMovieAddedToList(movieDetails, listType);
    }
}
