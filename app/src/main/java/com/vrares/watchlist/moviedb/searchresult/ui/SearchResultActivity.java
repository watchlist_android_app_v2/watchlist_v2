package com.vrares.watchlist.moviedb.searchresult.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.f2prateek.dart.InjectExtra;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.movielist.Movie;
import com.vrares.watchlist.core.injections.base.ActivityWithInjections;
import com.vrares.watchlist.moviedb.searchresult.presentation.SearchResultAdapter;
import com.vrares.watchlist.moviedb.searchresult.presentation.SearchResultContract;
import com.vrares.watchlist.moviedb.searchresult.presentation.SearchResultPresenter;
import com.vrares.watchlist.moviedb.watch.presentation.WatchListAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class SearchResultActivity extends ActivityWithInjections implements SearchResultContract.View {

    @BindView(R.id.movie_search_bar_result_screen) View searchBarLayout;
    @BindView(R.id.search_result_list) RecyclerView searchResultList;
    @BindView(R.id.search_result_loading) ProgressBar loadingBar;
    @BindView(R.id.search_result_title) TextView searchTitle;
    @BindView(R.id.no_search_results) TextView noResultsText;

    @Inject SearchResultPresenter presenter;

    @InjectExtra String query;

    private Button searchBtn;
    private EditText movieSearchInput;
    private SearchResultAdapter watchListAdapter;
    private List<Movie> movies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attach(this);
    }

    @Override
    protected void onStop() {
        presenter.detach();
        super.onStop();
    }

    private void init() {
        searchBtn = searchBarLayout.findViewById(R.id.movie_search_btn);
        movieSearchInput = searchBarLayout.findViewById(R.id.movie_search_input);
        searchBtn.setOnClickListener(new SearchListener());
        searchTitle.setText("Searching for: " + query);
        presenter.searchForMovie(query);
    }

    @Override
    public int getContentView() {
        return R.layout.activity_search_result;
    }

    @Override
    public void onMovieListReceived(List<Movie> movies) {
        loadingBar.setVisibility(View.GONE);
        if (movies.size() > 0) {
            searchResultList.setVisibility(View.VISIBLE);
            noResultsText.setVisibility(View.GONE);
        } else {
            noResultsText.setVisibility(View.VISIBLE);
        }
        this.movies = movies;
        watchListAdapter = new SearchResultAdapter(movies, this);
        searchResultList.setAdapter(watchListAdapter);
        searchResultList.setLayoutManager(new LinearLayoutManager(this));
        watchListAdapter.notifyDataSetChanged();
    }

    private class SearchListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            searchTitle.setText("Searching for: " + movieSearchInput.getText().toString());
            searchResultList.setVisibility(View.GONE);
            loadingBar.setVisibility(View.VISIBLE);
            noResultsText.setVisibility(View.GONE);
            presenter.searchForMovie(movieSearchInput.getText().toString());
        }
    }
}
