package com.vrares.watchlist.moviedb.moviedetails.presentation;

import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieStats;
import com.vrares.watchlist.core.entities.moviedetails.MovieSummary;
import com.vrares.watchlist.moviedb.moviedetails.usecase.AddToListUseCase;
import com.vrares.watchlist.moviedb.moviedetails.usecase.GetMovieDetailsUseCase;
import com.vrares.watchlist.moviedb.watch.usecase.RemoveFromListUseCase;

import javax.inject.Inject;

public class MovieDetailsPresenter implements MovieDetailsContract.Presenter {

    @Inject GetMovieDetailsUseCase getMovieDetailsUseCase;
    @Inject RemoveFromListUseCase removeFromListUseCase;
    @Inject AddToListUseCase addToListUseCase;

    private MovieDetailsContract.View view;
    private MovieDetails movieDetails;
    private MovieStats movieStats;

    public void attach(MovieDetailsContract.View view) {
        this.view = view;
    }

    public void detach() {
        this.view = null;
    }

    public void getMovieDetails(int movieId) {
        getMovieDetailsUseCase.getMovieDetails(movieId, this);
        getMovieDetailsUseCase.getDatabaseDetails(movieId, this);
    }

    public void addMovieToList(MovieDetails movieDetails, String listType) {
        addToListUseCase.addMovieToList(movieDetails, listType, this);
    }

    private boolean didAllDetailsReceived() {
        return movieDetails != null && movieStats != null;
    }

    public void removeFromList(MovieDetails movieDetails, String listType) {
        removeFromListUseCase.removeFromList(movieDetails, listType);
    }

    @Override
    public void onMovieDetailsReceived(MovieDetails movieDetails) {
        this.movieDetails = movieDetails;
        if (didAllDetailsReceived()) {
            view.onMovieDetailsReceived(new MovieSummary(movieStats, movieDetails));
        }
    }

    @Override
    public void onMovieAddedToList(String listType) {
        view.onMovieAddedToList(listType);
    }

    @Override
    public void onMovieDetailsReceived(MovieStats movieStats) {
        this.movieStats = movieStats;
        if (didAllDetailsReceived()) {
            view.onMovieDetailsReceived(new MovieSummary(movieStats, movieDetails));
        }
    }
}
