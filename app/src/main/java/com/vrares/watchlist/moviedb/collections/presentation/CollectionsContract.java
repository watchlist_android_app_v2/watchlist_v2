package com.vrares.watchlist.moviedb.collections.presentation;

import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;

import java.util.ArrayList;

public interface CollectionsContract {

    interface View {

        void onEmptyCollection();

        void onCollectionReceived(ArrayList<MovieWatched> movieCollection);
    }

    interface Presenter {

        void onCollectionReceived(ArrayList<MovieWatched> movieCollection);

        void onEmptyCollection();
    }

}
