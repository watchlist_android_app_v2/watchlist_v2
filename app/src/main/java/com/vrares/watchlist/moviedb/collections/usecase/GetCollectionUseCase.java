package com.vrares.watchlist.moviedb.collections.usecase;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.entities.moviedetails.MovieSummary;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.moviedb.collections.presentation.CollectionsContract;
import com.vrares.watchlist.social.usecase.get.GetUserInfoCallback;

import java.util.ArrayList;

import javax.inject.Singleton;

import static com.vrares.watchlist.core.Constants.COLLECTION;
import static com.vrares.watchlist.core.Constants.FAVOURITES;

@Singleton
public class GetCollectionUseCase {

    public void getCollection(final CollectionsContract.Presenter presenter) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference()
                .child("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<MovieWatched> movieCollection = new ArrayList<>();
                for (DataSnapshot movieSnapshot : dataSnapshot.child(COLLECTION).getChildren()) {
                    MovieWatched movieWatched = movieSnapshot.getValue(MovieWatched.class);
                    if (dataSnapshot.child(FAVOURITES).hasChild(movieSnapshot.getKey()) && movieWatched != null) {
                        movieWatched.isFavourite = true;
                    }
                    movieCollection.add(movieWatched);
                }

                if (movieCollection.isEmpty()) {
                    presenter.onEmptyCollection();
                } else {
                    presenter.onCollectionReceived(movieCollection);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("Error", databaseError.getMessage());
            }
        });
    }

    public void getCollection(final GetUserInfoCallback callback) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference()
                .child("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(COLLECTION);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<MovieWatched> movieCollection = new ArrayList<>();
                for (DataSnapshot movieSnapshot : dataSnapshot.getChildren()) {
                    movieCollection.add(movieSnapshot.getValue(MovieWatched.class));
                }
                callback.onCollectionReceived(movieCollection);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("Error", databaseError.getMessage());
            }
        });
    }
}
