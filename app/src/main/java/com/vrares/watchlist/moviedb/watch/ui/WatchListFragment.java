package com.vrares.watchlist.moviedb.watch.ui;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vrares.watchlist.R;
import com.vrares.watchlist.core.Constants;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.injections.base.FragmentWithInjections;
import com.vrares.watchlist.moviedb.watch.presentation.WatchListAdapter;
import com.vrares.watchlist.moviedb.watch.presentation.WatchListContract;
import com.vrares.watchlist.moviedb.watch.presentation.WatchListPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

import static com.vrares.watchlist.core.Constants.COLLECTION;
import static com.vrares.watchlist.core.Constants.FAVOURITES;
import static com.vrares.watchlist.core.Constants.WATCHLIST;

public class WatchListFragment extends FragmentWithInjections implements WatchListContract.View {

    @Inject WatchListPresenter presenter;

    @BindView(R.id.rv_watch_list) RecyclerView rvWatchList;
    @BindView(R.id.watch_list_loading) ProgressBar pbLoading;
    @BindView(R.id.tv_empty_watch_list) TextView tvEmptyWatchList;
    @BindView(R.id.watchlist_title) TextView watchlistTitle;

    private ArrayList<MovieDetails> watchList;
    private ArrayList<MovieDetails> favourites;
    private WatchListAdapter adapter;

    public WatchListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_watch_list, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.attach(this);
        presenter.getLists();
    }

    @Override
    public void onStop() {
        presenter.detach();
        super.onStop();
    }

    @Override
    public void onWatchListEmpty() {
        pbLoading.setVisibility(View.GONE);
        tvEmptyWatchList.setVisibility(View.VISIBLE);
    }

    @Override
    public void onWatchListReceived(ArrayList<MovieDetails> watchList, ArrayList<MovieDetails> favourites) {
        this.watchList = watchList;
        this.favourites = favourites;
        watchlistTitle.setText(String.format(getString(R.string.your_watchlist), String.valueOf(watchList.size())));

        adapter = new WatchListAdapter(watchList, favourites, getActivity(), this);
        rvWatchList.setAdapter(adapter);
        rvWatchList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter.notifyDataSetChanged();

        pbLoading.setVisibility(View.GONE);
        rvWatchList.setVisibility(View.VISIBLE);
        watchlistTitle.setVisibility(View.VISIBLE);
    }

    @Override
    public void addMovieToFavourites(MovieDetails movie) {
        presenter.addMovieToList(movie, FAVOURITES);
    }

    @Override
    public void addMovieToCollection(MovieDetails movie) {
        presenter.addMovieToList(movie, COLLECTION);
    }

    @Override
    public void onMovieAddedToList(MovieDetails movieDetails, String listType) {
        Toast.makeText(getActivity(), "Movie added to " + listType, Toast.LENGTH_SHORT).show();
        if (listType.equals(COLLECTION)) {
            watchList.remove(movieDetails);
            watchlistTitle.setText(String.format(getString(R.string.your_watchlist), String.valueOf(watchList.size())));
            adapter.notifyDataSetChanged();

            if (watchList.size() < 1) {
                watchlistTitle.setVisibility(View.GONE);
                rvWatchList.setVisibility(View.GONE);
                tvEmptyWatchList.setVisibility(View.VISIBLE);
            }

            presenter.removeFromList(movieDetails, WATCHLIST);
        }
    }

    @Override
    public void removeFromFavourites(MovieDetails movie) {
        Toast.makeText(getActivity(), "Movie removed from " + FAVOURITES, Toast.LENGTH_SHORT).show();
        presenter.removeFromList(movie, FAVOURITES);
    }
}
