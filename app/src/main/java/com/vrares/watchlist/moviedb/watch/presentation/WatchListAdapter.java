package com.vrares.watchlist.moviedb.watch.presentation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.movielist.Movie;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WatchListAdapter extends RecyclerView.Adapter<WatchListAdapter.MyViewHolder> {

    private static final String SIZE = "w92/";
    private static final String IMG_URL = "http://image.tmdb.org/t/p/" + SIZE;

    private final List<MovieDetails> watchList;
    private List<MovieDetails> favourites;
    private final Context context;
    private WatchListContract.View view;

    public WatchListAdapter(ArrayList<MovieDetails> watchList, ArrayList<MovieDetails> favourites, Context context, WatchListContract.View view) {
        this.watchList = watchList;
        this.favourites = favourites;
        this.context = context;
        this.view = view;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_watch_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final MovieDetails movie = watchList.get(position);

        if (isMovieIsInFavourites(movie)) {
            holder.favBtn.setBackgroundResource(R.drawable.btn_added_to_fav);
        }

        Glide.with(context)
                .load(IMG_URL + movie.getPosterPath())
                .into(holder.ivPoster);

        holder.tvTitle.setText(movie.getTitle());
        holder.tvRelease.setText(movie.getReleaseDate());

        holder.seeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.seeBtn.setBackgroundResource(R.drawable.see_btn_seen);
                view.addMovieToCollection(movie);
            }
        });

        holder.favBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isInFavourites = false;
                for (MovieDetails favouriteMovie : favourites) {
                    if (favouriteMovie.getId().equals(movie.getId())) {
                        isInFavourites = true;
                    }
                }
                if (isInFavourites) {
                    MovieDetails movieToDelete = null;
                    for (MovieDetails favouriteMovie : favourites) {
                        if (favouriteMovie.getId().equals(movie.getId())) {
                            movieToDelete = favouriteMovie;
                        }
                    }
                    favourites.remove(movieToDelete);
                    holder.favBtn.setBackgroundResource(R.drawable.btn_not_fav);
                    view.removeFromFavourites(movie);
                } else {
                    favourites.add(movie);
                    holder.favBtn.setBackgroundResource(R.drawable.btn_added_to_fav);
                    view.addMovieToFavourites(movie);
                }
            }
        });
    }

    private boolean isMovieIsInFavourites(MovieDetails movie) {
        for (MovieDetails favouriteMovie : favourites) {
            if (favouriteMovie.getId().equals(movie.getId())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return watchList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_watch_list_poster) ImageView ivPoster;
        @BindView(R.id.item_watch_list_title) TextView tvTitle;
        @BindView(R.id.item_watch_list_release) TextView tvRelease;
        @BindView(R.id.see_btn) Button seeBtn;
        @BindView(R.id.favourite_btn) Button favBtn;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
