package com.vrares.watchlist.moviedb.searchresult.presentation;

import com.vrares.watchlist.core.entities.movielist.Movie;

import java.util.List;

public interface SearchResultContract {

    interface View {

        void onMovieListReceived(List<Movie> movies);
    }

    interface Presenter {

        void onMovieListReceived(List<Movie> movies);
    }

}
