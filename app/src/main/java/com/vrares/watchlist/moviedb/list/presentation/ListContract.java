package com.vrares.watchlist.moviedb.list.presentation;

import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;

import java.util.ArrayList;

public interface ListContract {

    interface View {

        void onCollectionReceived(ArrayList<MovieWatched> movies);

        void onListReceived(ArrayList<MovieDetails> movies);
    }

    interface Presenter {

        void onCollectionReceived(ArrayList<MovieWatched> movies);

        void onListReceived(ArrayList<MovieDetails> movies);
    }

}
