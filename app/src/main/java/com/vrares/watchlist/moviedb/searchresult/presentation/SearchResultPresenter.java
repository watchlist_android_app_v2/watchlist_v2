package com.vrares.watchlist.moviedb.searchresult.presentation;

import com.vrares.watchlist.core.entities.movielist.Movie;
import com.vrares.watchlist.moviedb.searchresult.usecase.SearchForMovieUseCase;

import java.util.List;

import javax.inject.Inject;

public class SearchResultPresenter implements SearchResultContract.Presenter{

    @Inject SearchForMovieUseCase searchForMovieUseCase;

    private SearchResultContract.View view;

    public void attach(SearchResultContract.View view) {
        this.view = view;
    }

    public void detach() {
        this.view = null;
    }

    // --------------------- CALLS ----------------------------------------

    public void searchForMovie(String searchQuery) {
        searchForMovieUseCase.searchForMovie(searchQuery, this);
    }


    // --------------------- CALLBACKS ----------------------------------------

    @Override
    public void onMovieListReceived(List<Movie> movies) {
        view.onMovieListReceived(movies);
    }
}
