package com.vrares.watchlist.moviedb.collections.ui;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.core.injections.base.FragmentWithInjections;
import com.vrares.watchlist.moviedb.collections.presentation.CollectionsAdapter;
import com.vrares.watchlist.moviedb.collections.presentation.CollectionsContract;
import com.vrares.watchlist.moviedb.collections.presentation.CollectionsPresenter;

import java.util.ArrayList;
import javax.inject.Inject;

import butterknife.BindView;

public class CollectionsFragment extends FragmentWithInjections implements CollectionsContract.View {

    @Inject CollectionsPresenter presenter;
    @BindView(R.id.rv_collection) RecyclerView rvCollection;
    @BindView(R.id.collection_title) TextView collectionTitle;
    @BindView(R.id.tv_empty_collection) TextView tvEmptyCollection;
    @BindView(R.id.collection_loading) ProgressBar collectionLoading;

    public CollectionsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_collections, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.attach(this);
        presenter.getCollection();

    }

    @Override
    public void onStop() {
        presenter.detach();
        super.onStop();
    }

    @Override
    public void onEmptyCollection() {
        collectionLoading.setVisibility(View.GONE);
        tvEmptyCollection.setVisibility(View.VISIBLE);
        collectionTitle.setVisibility(View.VISIBLE);
        collectionTitle.setText(String.format(getString(R.string.collection_title), "0"));
    }

    @Override
    public void onCollectionReceived(ArrayList<MovieWatched> movieCollection) {
        collectionLoading.setVisibility(View.GONE);
        collectionTitle.setVisibility(View.VISIBLE);
        collectionTitle.setText(String.format(getString(R.string.collection_title), String.valueOf(movieCollection.size())));

        CollectionsAdapter adapter = new CollectionsAdapter(movieCollection, getActivity());
        rvCollection.setAdapter(adapter);
        rvCollection.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter.notifyDataSetChanged();
        rvCollection.setVisibility(View.VISIBLE);
    }
}
