package com.vrares.watchlist.moviedb.watch.presentation;

import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;

import java.util.ArrayList;

public interface WatchListContract {

    interface View {

        void onWatchListEmpty();

        void onWatchListReceived(ArrayList<MovieDetails> watchList, ArrayList<MovieDetails> favourites);

        void addMovieToFavourites(MovieDetails movie);

        void addMovieToCollection(MovieDetails movie);

        void onMovieAddedToList(MovieDetails movieDetails, String listType);

        void removeFromFavourites(MovieDetails movie);
    }

    interface Presenter {

        void onWatchListEmpty();

        void onWatchListReceived(ArrayList<MovieDetails> watchList, ArrayList<MovieDetails> favourites);

        void onMovieAddedToList(MovieDetails movieDetails, String listType);
    }

}
