package com.vrares.watchlist.moviedb.home.presentation.homescreen;

import com.vrares.watchlist.core.entities.movielist.Movie;

import java.util.List;

public interface HomeContract {

    interface View {

        void onMovieListReceived(List<Movie> movies, int listMode);
    }

    interface Presenter {

        void onMovieListReceived(List<Movie> movies, int listMode);
    }

}
