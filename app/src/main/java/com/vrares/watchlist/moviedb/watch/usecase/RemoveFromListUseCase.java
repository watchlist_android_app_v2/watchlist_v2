package com.vrares.watchlist.moviedb.watch.usecase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;

import javax.inject.Singleton;

@Singleton
public class RemoveFromListUseCase {

    public void removeFromList(MovieDetails movieDetails, String listType) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(listType)
                .child(movieDetails.getId().toString());
        reference.removeValue();
    }
}
