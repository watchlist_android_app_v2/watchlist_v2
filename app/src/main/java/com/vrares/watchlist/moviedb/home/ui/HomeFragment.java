package com.vrares.watchlist.moviedb.home.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.vrares.watchlist.Henson;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.movielist.Movie;
import com.vrares.watchlist.core.injections.base.FragmentWithInjections;
import com.vrares.watchlist.moviedb.home.presentation.homescreen.HomeContract;
import com.vrares.watchlist.moviedb.home.presentation.homescreen.HomePresenter;
import com.vrares.watchlist.moviedb.home.presentation.homescreen.MovieListAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends FragmentWithInjections implements HomeContract.View {

    public static final int ALL_LISTS_MODE = 0;
    public static final int POPULAR_MODE = 1;
    public static final int TOP_RATED_MODE = 2;
    public static final int UPCOMING_MODE = 3;

    @BindView(R.id.movie_search_bar) View searchBarLayout;
    @BindView(R.id.top_rated_layout) View topRatedLayout;
    @BindView(R.id.popular_layout) View popularLayout;
    @BindView(R.id.upcoming_layout) View upcomingLayout;
    @BindView(R.id.home_loading) ProgressBar homeLoading;
    @BindView(R.id.home_content) ScrollView homeContent;

    @Inject HomePresenter presenter;

    private List<Movie> popularMovies;
    private List<Movie> topRatedMovies;
    private List<Movie> upcomingMovies;
    private RecyclerView rvTopRated;
    private RecyclerView rvPopular;
    private RecyclerView rvUpcoming;
    private Button searchBtn;
    private EditText movieSearchInput;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.attach(this);
        init();
    }

    private void init() {
        presenter.getMovieList(1, ALL_LISTS_MODE);
        rvTopRated = topRatedLayout.findViewById(R.id.rv_top_rated);
        rvPopular = popularLayout.findViewById(R.id.rv_popular);
        rvUpcoming = upcomingLayout.findViewById(R.id.rv_upcoming);
        searchBtn = searchBarLayout.findViewById(R.id.movie_search_btn);
        movieSearchInput = searchBarLayout.findViewById(R.id.movie_search_input);
        movieSearchInput.setText("");

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(getActivity())
                        .gotoSearchResultActivity()
                        .query(movieSearchInput.getText().toString())
                        .build();
                getActivity().startActivity(intent);
            }
        });
    }

    @Override
    public void onStop() {
        presenter.detach();
        super.onStop();
    }

    @Override
    public void onMovieListReceived(List<Movie> movies, int listMode) {
        switch (listMode) {
            case POPULAR_MODE:
                popularMovies = movies;
                break;
            case TOP_RATED_MODE:
                topRatedMovies = movies;
                break;
            case UPCOMING_MODE:
                upcomingMovies = movies;
                break;
            default:
                Log.d("Error", "HomeFragment.onMovieListReceived()");
                break;
        }
        if (popularMovies != null && topRatedMovies != null && upcomingMovies != null) {
            setupMovieLists();
        }
    }

    private void setupMovieLists() {
        MovieListAdapter popularAdapter = new MovieListAdapter(popularMovies, getActivity());
        MovieListAdapter topRatedAdapter = new MovieListAdapter(topRatedMovies, getActivity());
        MovieListAdapter upcomingAdapter = new MovieListAdapter(upcomingMovies, getActivity());

        rvPopular.setAdapter(popularAdapter);
        rvTopRated.setAdapter(topRatedAdapter);
        rvUpcoming.setAdapter(upcomingAdapter);

        rvPopular.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvTopRated.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvUpcoming.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        popularAdapter.notifyDataSetChanged();
        topRatedAdapter.notifyDataSetChanged();
        upcomingAdapter.notifyDataSetChanged();
        homeContent.setVisibility(View.VISIBLE);
        homeLoading.setVisibility(View.GONE);
    }
}
