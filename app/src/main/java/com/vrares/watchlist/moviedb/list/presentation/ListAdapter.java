package com.vrares.watchlist.moviedb.list.presentation;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vrares.watchlist.Henson;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.vrares.watchlist.core.Constants.IMG_URL;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private ArrayList<MovieDetails> movies;

    public ListAdapter(ArrayList<MovieDetails> movies) {
        this.movies = movies;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_watch_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MovieDetails movie = movies.get(position);

        holder.favBtn.setVisibility(View.GONE);
        holder.seeBtn.setVisibility(View.GONE);

        Glide.with(holder.itemView.getContext())
                .load(IMG_URL + movie.getPosterPath())
                .into(holder.ivPoster);

        holder.tvTitle.setText(movie.getTitle());
        holder.tvRelease.setText(movie.getReleaseDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(holder.itemView.getContext())
                        .gotoMovieDetailsActivity()
                        .movieId(movie.getId())
                        .build();
                holder.itemView.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_watch_list_poster)
        ImageView ivPoster;
        @BindView(R.id.item_watch_list_title)
        TextView tvTitle;
        @BindView(R.id.item_watch_list_release) TextView tvRelease;
        @BindView(R.id.see_btn)
        Button seeBtn;
        @BindView(R.id.favourite_btn) Button favBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
