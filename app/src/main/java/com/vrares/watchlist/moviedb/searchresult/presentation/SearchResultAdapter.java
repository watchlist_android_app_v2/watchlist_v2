package com.vrares.watchlist.moviedb.searchresult.presentation;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vrares.watchlist.Henson;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.movielist.Movie;
import com.vrares.watchlist.moviedb.watch.presentation.WatchListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.MyViewHolder> {

    private static final String SIZE = "w92/";
    private static final String IMG_URL = "http://image.tmdb.org/t/p/" + SIZE;

    private final List<Movie> watchList;
    private final Context context;

    public SearchResultAdapter(List<Movie> watchList, Context context) {
        this.watchList = watchList;
        this.context = context;
    }

    @Override
    public SearchResultAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_watch_list, parent, false);
        return new SearchResultAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SearchResultAdapter.MyViewHolder holder, int position) {
        final Movie movie = watchList.get(position);

        holder.favBtn.setVisibility(View.GONE);
        holder.seeBtn.setVisibility(View.GONE);

        Glide.with(context)
                .load(IMG_URL + movie.getPosterPath())
                .into(holder.ivPoster);

        holder.tvTitle.setText(movie.getTitle());
        holder.tvRelease.setText(movie.getReleaseDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(holder.itemView.getContext())
                        .gotoMovieDetailsActivity()
                        .movieId(movie.getId())
                        .build();
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return watchList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_watch_list_poster) ImageView ivPoster;
        @BindView(R.id.item_watch_list_title) TextView tvTitle;
        @BindView(R.id.item_watch_list_release) TextView tvRelease;
        @BindView(R.id.see_btn) Button seeBtn;
        @BindView(R.id.favourite_btn) Button favBtn;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
