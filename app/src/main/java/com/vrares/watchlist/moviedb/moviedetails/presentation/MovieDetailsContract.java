package com.vrares.watchlist.moviedb.moviedetails.presentation;

import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieStats;
import com.vrares.watchlist.core.entities.moviedetails.MovieSummary;

public interface MovieDetailsContract {

    interface View {

        void onMovieDetailsReceived(MovieSummary movieSummary);

        void onMovieAddedToList(String listType);
    }

    interface Presenter {

        void onMovieDetailsReceived(MovieDetails body);

        void onMovieAddedToList(String listType);

        void onMovieDetailsReceived(MovieStats movieStats);
    }

}
