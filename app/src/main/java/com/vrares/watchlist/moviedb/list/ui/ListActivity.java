package com.vrares.watchlist.moviedb.list.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.f2prateek.dart.InjectExtra;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.core.injections.base.ActivityWithInjections;
import com.vrares.watchlist.moviedb.list.presentation.ListAdapter;
import com.vrares.watchlist.moviedb.list.presentation.ListContract;
import com.vrares.watchlist.moviedb.list.presentation.ListPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

public class ListActivity extends ActivityWithInjections implements ListContract.View {

    @Inject ListPresenter presenter;
    @InjectExtra String userId;
    @InjectExtra String listType;
    @InjectExtra String userName;
    @InjectExtra String listSize;

    @BindView(R.id.list_title_user) TextView listTitle;
    @BindView(R.id.list_recycler_view) RecyclerView list;
    @BindView(R.id.loading_list_user) ProgressBar loading;

    private ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listTitle.setText(String.format(getString(R.string.user_list_title), listType, userName));
        presenter.getList(userId, listType);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attach(this);
    }

    @Override
    protected void onStop() {
        presenter.detach();
        super.onStop();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_list;
    }

    @Override
    public void onCollectionReceived(ArrayList<MovieWatched> movies) {
        ArrayList<MovieDetails> movieDetails = new ArrayList<>();
        for (MovieWatched movie : movies) {
             movieDetails.add(movie.movieDetails);
        }
        listAdapter = new ListAdapter(movieDetails);
        list.setAdapter(listAdapter);
        list.setLayoutManager(new LinearLayoutManager(this));
        listAdapter.notifyDataSetChanged();
        loading.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);
    }

    @Override
    public void onListReceived(ArrayList<MovieDetails> movies) {
        listAdapter = new ListAdapter(movies);
        list.setAdapter(listAdapter);
        list.setLayoutManager(new LinearLayoutManager(this));
        listAdapter.notifyDataSetChanged();
        loading.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);
    }
}
