package com.vrares.watchlist.moviedb.watch.usecase;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.moviedb.watch.presentation.WatchListContract;
import com.vrares.watchlist.social.usecase.get.GetUserInfoCallback;

import java.util.ArrayList;
import javax.inject.Singleton;

import static com.vrares.watchlist.core.Constants.FAVOURITES;
import static com.vrares.watchlist.core.Constants.WATCHLIST;

@Singleton
public class GetWatchListUseCase {

    public void getWatchList(final WatchListContract.Presenter presenter) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final String userId = auth.getCurrentUser().getUid();

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<MovieDetails> watchList = new ArrayList<>();
                if (dataSnapshot.hasChild(userId)) {
                    for (DataSnapshot snapshot : dataSnapshot.child(userId).child(WATCHLIST).getChildren()) {
                        watchList.add(snapshot.getValue(MovieDetails.class));
                    }
                }
                ArrayList<MovieDetails> favourites = new ArrayList<>();
                if (dataSnapshot.hasChild(userId)) {
                    for (DataSnapshot snapshot : dataSnapshot.child(userId).child(FAVOURITES).getChildren()) {
                        favourites.add(snapshot.getValue(MovieDetails.class));
                    }
                }
                if (watchList.isEmpty()) {
                    presenter.onWatchListEmpty();
                } else {
                    presenter.onWatchListReceived(watchList, favourites);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("Error", databaseError.getMessage());
            }
        });
    }

    public void getWatchList(final GetUserInfoCallback callback) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final String userId = auth.getCurrentUser().getUid();

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<MovieDetails> watchList = new ArrayList<>();
                if (dataSnapshot.hasChild(userId)) {
                    for (DataSnapshot snapshot : dataSnapshot.child(userId).child(WATCHLIST).getChildren()) {
                        watchList.add(snapshot.getValue(MovieDetails.class));
                    }
                }
                ArrayList<MovieDetails> favourites = new ArrayList<>();
                if (dataSnapshot.hasChild(userId)) {
                    for (DataSnapshot snapshot : dataSnapshot.child(userId).child(FAVOURITES).getChildren()) {
                        favourites.add(snapshot.getValue(MovieDetails.class));
                    }
                }
                callback.onWatchListAndFavouritesReceived(watchList, favourites);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("Error", databaseError.getMessage());
            }
        });
    }
}
