package com.vrares.watchlist.moviedb.collections.presentation;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vrares.watchlist.Henson;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.core.utils.MovieUtil;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CollectionsAdapter extends RecyclerView.Adapter<CollectionsAdapter.MyViewHolder> {

    private static final String SIZE = "w92/";
    private static final String IMG_URL = "http://image.tmdb.org/t/p/" + SIZE;

    private final ArrayList<MovieWatched> collectionList;
    private final Context context;

    public CollectionsAdapter(ArrayList<MovieWatched> collectionList, Context context) {
        this.collectionList = collectionList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_collection, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final MovieWatched movie = collectionList.get(position);

        holder.isFavImage.setVisibility(movie.isFavourite ? View.VISIBLE : View.GONE);

        Glide.with(context)
                .load(IMG_URL + movie.movieDetails.getPosterPath())
                .into(holder.ivPoster);

        holder.tvTitle.setText(movie.movieDetails.getOriginalTitle());
        holder.tvTimestamp.setText(formattedDate(movie.timestamp));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(context)
                        .gotoMovieDetailsActivity()
                        .movieId(movie.movieDetails.getId())
                        .build();
                context.startActivity(intent);
            }
        });
    }

    private String formattedDate(String timestamp) {
        Timestamp stamp = Timestamp.valueOf(timestamp);
        Date date = new Date(stamp.getTime());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        return dateFormat.format(date);
    }

    @Override
    public int getItemCount() {
        return collectionList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_collection_poster) ImageView ivPoster;
        @BindView(R.id.item_collection_title) TextView tvTitle;
        @BindView(R.id.item_collection_rating) TextView tvTimestamp;
        @BindView(R.id.is_fav_image) ImageView isFavImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
