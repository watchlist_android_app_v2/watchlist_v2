package com.vrares.watchlist.moviedb.moviedetails.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.f2prateek.dart.InjectExtra;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.moviedetails.MovieSummary;
import com.vrares.watchlist.core.injections.base.ActivityWithInjections;
import com.vrares.watchlist.core.utils.MovieUtil;
import com.vrares.watchlist.moviedb.moviedetails.presentation.MovieDetailsContract;
import com.vrares.watchlist.moviedb.moviedetails.presentation.MovieDetailsPresenter;

import javax.inject.Inject;

import butterknife.BindView;

import static com.vrares.watchlist.core.Constants.COLLECTION;
import static com.vrares.watchlist.core.Constants.FAVOURITES;
import static com.vrares.watchlist.core.Constants.IMG_URL;
import static com.vrares.watchlist.core.Constants.TMDB_URL;
import static com.vrares.watchlist.core.Constants.WATCHLIST;

public class MovieDetailsActivity extends ActivityWithInjections implements MovieDetailsContract.View, View.OnClickListener {



    @BindView(R.id.movie_details_loading) ProgressBar loading;
    @BindView(R.id.movie_details_content) View content;

    @BindView(R.id.movie_details_backdrop) ImageView backdrop;
    @BindView(R.id.movie_poster) ImageView poster;
    @BindView(R.id.movie_title) TextView title;
    @BindView(R.id.release_year) TextView releaseYear;
    @BindView(R.id.add_watchlist_btn) Button watchlistBtn;
    @BindView(R.id.see_btn) Button seeBtn;
    @BindView(R.id.favourite_btn) Button favBtn;
    @BindView(R.id.share_btn) Button shareBtn;
    @BindView(R.id.genres_title_content) TextView genres;
    @BindView(R.id.overview_content) TextView overview;

    @Inject MovieDetailsPresenter presenter;
    @Inject MovieUtil movieUtil;

    @InjectExtra int movieId;

    private MovieSummary movieSummary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attach(this);
        presenter.getMovieDetails(movieId);
    }

    @Override
    protected void onStop() {
        presenter.detach();
        super.onStop();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_movie_details;
    }

    @Override
    public void onMovieDetailsReceived(MovieSummary movieSummary) {
        this.movieSummary = movieSummary;
        Glide.with(this)
                .load(IMG_URL + movieSummary.movieDetails.getBackdropPath())
                .into(backdrop);
        Glide.with(this)
                .load(IMG_URL + movieSummary.movieDetails.getPosterPath())
                .into(poster);
        title.setText(movieSummary.movieDetails.getTitle());
        releaseYear.setText(movieUtil.getReleaseYear(movieSummary.movieDetails.getReleaseDate()));
        overview.setText(movieSummary.movieDetails.getOverview());
        genres.setText(movieUtil.getGenres(movieSummary.movieDetails.getGenres()));

        favBtn.setOnClickListener(this);
        seeBtn.setOnClickListener(this);
        watchlistBtn.setOnClickListener(this);
        shareBtn.setOnClickListener(this);

        checkLists();
        loading.setVisibility(View.GONE);
        content.setVisibility(View.VISIBLE);
    }

    private void checkLists() {
        if (movieSummary.movieStats.isFavourite) {
            favBtn.setBackgroundResource(R.drawable.btn_added_to_fav);
        }
        if (movieSummary.movieStats.isInWatchlist) {
            watchlistBtn.setBackgroundResource(R.drawable.ic_added_to_watchlist_btn);
        }
        if (movieSummary.movieStats.isInCollection) {
            seeBtn.setBackgroundResource(R.drawable.see_btn_seen);
        }
    }

    @Override
    public void onMovieAddedToList(String listType) {
        Toast.makeText(this, "Movie added to " + listType, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_watchlist_btn:
                if (movieSummary.movieStats.isInWatchlist) {
                    movieSummary.movieStats.isInWatchlist = false;
                    watchlistBtn.setBackgroundResource(R.drawable.ic_watchlist_btn);
                    presenter.removeFromList(movieSummary.movieDetails, WATCHLIST);
                    displayListRemovalCallback(WATCHLIST);
                } else {
                    movieSummary.movieStats.isInWatchlist = true;
                    watchlistBtn.setBackgroundResource(R.drawable.ic_added_to_watchlist_btn);
                    presenter.addMovieToList(movieSummary.movieDetails, WATCHLIST);
                }
                break;
            case R.id.see_btn:
                if (movieSummary.movieStats.isInCollection) {
                    movieSummary.movieStats.isInCollection = false;
                    seeBtn.setBackgroundResource(R.drawable.see_btn_not_seen);
                    presenter.removeFromList(movieSummary.movieDetails, COLLECTION);
                    displayListRemovalCallback(COLLECTION);
                } else {
                    movieSummary.movieStats.isInCollection = true;
                    seeBtn.setBackgroundResource(R.drawable.see_btn_seen);
                    presenter.addMovieToList(movieSummary.movieDetails, COLLECTION);
                }
                break;
            case R.id.favourite_btn:
                if (movieSummary.movieStats.isFavourite) {
                    movieSummary.movieStats.isFavourite = false;
                    favBtn.setBackgroundResource(R.drawable.btn_not_fav);
                    presenter.removeFromList(movieSummary.movieDetails, FAVOURITES);
                    displayListRemovalCallback(FAVOURITES);
                } else {
                    movieSummary.movieStats.isFavourite = true;
                    favBtn.setBackgroundResource(R.drawable.btn_added_to_fav);
                    presenter.addMovieToList(movieSummary.movieDetails, FAVOURITES);
                }
                break;
            case R.id.share_btn:
                Intent shareIntent = new Intent();
                shareIntent.setType("text/plain");
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, movieSummary.movieDetails.getOriginalTitle());
                shareIntent.putExtra(Intent.EXTRA_TEXT, TMDB_URL + movieId);
                startActivity(Intent.createChooser(shareIntent, movieSummary.movieDetails.getTitle()));
        }
    }

    private void displayListRemovalCallback(String listType) {
        Toast.makeText(this, "Removed from " + listType, Toast.LENGTH_SHORT).show();
    }
}
