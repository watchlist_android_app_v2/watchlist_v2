package com.vrares.watchlist.moviedb.home.usecase;

import com.vrares.watchlist.moviedb.home.presentation.homescreen.HomeContract;
import com.vrares.watchlist.core.retrofit.RetrofitHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class GetMovieListsUseCase {

    @Inject RetrofitHelper retrofitHelper;

    public void getMovieList(int page, int listMode, HomeContract.Presenter callback) {
        retrofitHelper.getMovieList(page, listMode, callback);
    }
}
