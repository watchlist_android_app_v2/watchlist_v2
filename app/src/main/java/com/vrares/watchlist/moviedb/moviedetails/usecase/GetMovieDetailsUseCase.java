package com.vrares.watchlist.moviedb.moviedetails.usecase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.entities.moviedetails.MovieStats;
import com.vrares.watchlist.core.retrofit.RetrofitHelper;
import com.vrares.watchlist.moviedb.moviedetails.presentation.MovieDetailsContract;
import com.vrares.watchlist.moviedb.moviedetails.presentation.MovieDetailsPresenter;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.vrares.watchlist.core.Constants.COLLECTION;
import static com.vrares.watchlist.core.Constants.FAVOURITES;
import static com.vrares.watchlist.core.Constants.WATCHLIST;

@Singleton
public class GetMovieDetailsUseCase {

    @Inject RetrofitHelper retrofitHelper;

    public void getMovieDetails(int movieId, MovieDetailsContract.Presenter callback) {
        retrofitHelper.getMovieDetails(movieId, callback);
    }

    public void getDatabaseDetails(final int movieId, final MovieDetailsContract.Presenter callback) {
        DatabaseReference reference = FirebaseDatabase.getInstance()
                .getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        final MovieStats movieStats = new MovieStats();
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(COLLECTION).hasChild(String.valueOf(movieId))) {
                    movieStats.isInCollection = true;
                }
                if (dataSnapshot.child(WATCHLIST).hasChild(String.valueOf(movieId))) {
                    movieStats.isInWatchlist = true;
                }
                if (dataSnapshot.child(FAVOURITES).hasChild(String.valueOf(movieId))) {
                    movieStats.isFavourite = true;
                }

                callback.onMovieDetailsReceived(movieStats);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
