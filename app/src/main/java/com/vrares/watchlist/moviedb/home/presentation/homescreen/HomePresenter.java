package com.vrares.watchlist.moviedb.home.presentation.homescreen;

import com.vrares.watchlist.core.entities.movielist.Movie;
import com.vrares.watchlist.moviedb.home.usecase.GetMovieListsUseCase;
import com.vrares.watchlist.moviedb.searchresult.usecase.SearchForMovieUseCase;

import java.util.List;
import javax.inject.Inject;

public class HomePresenter implements HomeContract.Presenter {

    @Inject GetMovieListsUseCase getMovieListsUseCase;
    @Inject SearchForMovieUseCase searchForMovieUseCase;

    private HomeContract.View view;

    public void attach(HomeContract.View view) {
        this.view = view;
    }

    public void detach() {
        this.view = null;
    }



    // --------------------- CALLS ----------------------------------------

    public void getMovieList(int page, int listMode) {
        getMovieListsUseCase.getMovieList(page, listMode, this);
    }



    // --------------------- CALLBACKS ----------------------------------------

    @Override
    public void onMovieListReceived(List<Movie> movies, int listMode) {
        if (view != null) {
            view.onMovieListReceived(movies, listMode);
        }
    }
}
