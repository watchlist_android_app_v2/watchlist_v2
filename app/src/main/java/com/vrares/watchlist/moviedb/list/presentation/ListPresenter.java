package com.vrares.watchlist.moviedb.list.presentation;

import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.moviedb.list.usecase.GetListUseCase;

import java.util.ArrayList;

import javax.inject.Inject;

public class ListPresenter implements ListContract.Presenter {

    @Inject GetListUseCase getListUseCase;

    private ListContract.View view;

    public void attach(ListContract.View view) {
        this.view = view;
    }

    public void detach() {
        this.view = null;
    }

    public void getList(String userId, String listType) {
        getListUseCase.getList(userId, listType, this);
    }

    @Override
    public void onCollectionReceived(ArrayList<MovieWatched> movies) {
        view.onCollectionReceived(movies);
    }

    @Override
    public void onListReceived(ArrayList<MovieDetails> movies) {
        view.onListReceived(movies);
    }
}
