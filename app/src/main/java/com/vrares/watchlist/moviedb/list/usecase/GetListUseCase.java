package com.vrares.watchlist.moviedb.list.usecase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.moviedb.list.presentation.ListContract;
import com.vrares.watchlist.moviedb.list.presentation.ListPresenter;

import java.util.ArrayList;

import javax.inject.Singleton;

import static com.vrares.watchlist.core.Constants.COLLECTION;
import static com.vrares.watchlist.core.Constants.USERS;

@Singleton
public class GetListUseCase {

    public void getList(String userId, final String listType, final ListContract.Presenter callback) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(USERS).child(userId).child(listType);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (listType.equals(COLLECTION)) {
                    ArrayList<MovieWatched> movies = new ArrayList<>();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        movies.add(snapshot.getValue(MovieWatched.class));
                    }
                    callback.onCollectionReceived(movies);
                } else {
                    ArrayList<MovieDetails> movies = new ArrayList<>();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        movies.add(snapshot.getValue(MovieDetails.class));
                    }
                    callback.onListReceived(movies);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
