package com.vrares.watchlist.moviedb.searchresult.usecase;

import com.vrares.watchlist.core.retrofit.RetrofitHelper;
import com.vrares.watchlist.moviedb.searchresult.presentation.SearchResultContract;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SearchForMovieUseCase {

    @Inject RetrofitHelper retrofitHelper;

    public void searchForMovie(String searchQuery, SearchResultContract.Presenter callback) {
        retrofitHelper.searchForMovie(searchQuery, callback);
    }
}
