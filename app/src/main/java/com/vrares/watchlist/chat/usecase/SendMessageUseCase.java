package com.vrares.watchlist.chat.usecase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.vrares.watchlist.core.entities.social.ChatMessage;

import javax.inject.Singleton;

import static com.vrares.watchlist.core.Constants.CHAT;

@Singleton
public class SendMessageUseCase {

    public void sendMessage(String localChatRoom, String remoteChatRoom, ChatMessage chatMessage) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(CHAT);
        String chatId = reference.push().getKey().toString();
        chatMessage.id = chatId;

        reference.child(localChatRoom).child(chatId).setValue(chatMessage);
        reference.child(remoteChatRoom).child(chatId).setValue(chatMessage);

    }
}
