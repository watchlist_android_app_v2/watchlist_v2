package com.vrares.watchlist.chat.presentation;

import com.vrares.watchlist.core.entities.social.ChatMessage;

import java.util.ArrayList;

public interface ChatContract {

    interface View {

        void onMessageLoaded(ArrayList<ChatMessage> chatMessages);

        void onNewMessageLoaded(ChatMessage message);
    }

    interface Presenter {

        void onMessagesLoaded(ArrayList<ChatMessage> chatMessages);

        void onNewMessageLoaded(ChatMessage message);
    }

}
