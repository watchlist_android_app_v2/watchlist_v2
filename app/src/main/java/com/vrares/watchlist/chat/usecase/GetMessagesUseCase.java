package com.vrares.watchlist.chat.usecase;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.chat.presentation.ChatContract;
import com.vrares.watchlist.core.entities.social.ChatMessage;

import java.util.ArrayList;

import javax.inject.Singleton;

import static com.vrares.watchlist.core.Constants.CHAT;

@Singleton
public class GetMessagesUseCase {

    public void getMessages(String chatRoomKey, final ChatContract.Presenter callback) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(CHAT).child(chatRoomKey);

        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                callback.onNewMessageLoaded(dataSnapshot.getValue(ChatMessage.class));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<ChatMessage> chatMessages = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    chatMessages.add(snapshot.getValue(ChatMessage.class));
                }

                callback.onMessagesLoaded(chatMessages);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
