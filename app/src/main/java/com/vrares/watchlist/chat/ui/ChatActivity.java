package com.vrares.watchlist.chat.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.f2prateek.dart.InjectExtra;
import com.google.firebase.auth.FirebaseAuth;
import com.vrares.watchlist.R;
import com.vrares.watchlist.chat.presentation.ChatAdapter;
import com.vrares.watchlist.chat.presentation.ChatContract;
import com.vrares.watchlist.chat.presentation.ChatPresenter;
import com.vrares.watchlist.core.entities.social.ChatMessage;
import com.vrares.watchlist.core.injections.base.ActivityWithInjections;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends ActivityWithInjections implements ChatContract.View {

    @InjectExtra String userId;
    @InjectExtra String userName;
    @InjectExtra String userPicture;

    @BindView(R.id.chat_profile_picture) CircleImageView profilePicture;
    @BindView(R.id.chat_name) TextView userNameText;
    @BindView(R.id.message_input) EditText messageInput;
    @BindView(R.id.message_send_btn) Button sendMsgBtn;
    @BindView(R.id.chat_container) RecyclerView chatList;

    @Inject ChatPresenter presenter;

    private ArrayList<ChatMessage> messages;
    private ChatAdapter chatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attach(this);
        init();
        presenter.getMessages(FirebaseAuth.getInstance().getCurrentUser().getUid() + userId);
    }

    private void init() {
        messages = new ArrayList<>();
        chatAdapter = new ChatAdapter(messages);
        chatList.setAdapter(chatAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        layoutManager.setSmoothScrollbarEnabled(true);
        chatList.setLayoutManager(layoutManager);
        chatList.smoothScrollToPosition(messages.size());

        Glide.with(this)
                .load(userPicture)
                .into(profilePicture);
        userNameText.setText(userName);

        sendMsgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!messageInput.getText().toString().trim().equals("")) {
                    String localChatRoom = FirebaseAuth.getInstance().getCurrentUser().getUid() + userId;
                    String remoteChatRoom = userId + FirebaseAuth.getInstance().getCurrentUser().getUid();
                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.senderId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    chatMessage.text = messageInput.getText().toString();

                    messageInput.setText("");
                    presenter.sendMessage(localChatRoom, remoteChatRoom, chatMessage);
                }
            }
        });
    }

    @Override
    protected void onStop() {
        presenter.detach();
        super.onStop();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_chat;
    }

    @Override
    public void onMessageLoaded(ArrayList<ChatMessage> chatMessages) {
        this.messages = chatMessages;
        chatAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNewMessageLoaded(ChatMessage message) {
        chatAdapter.addMessage(message);
        if (chatList != null) {
            chatList.smoothScrollToPosition(chatAdapter.messages.size() + 1);
        }
    }
}
