package com.vrares.watchlist.chat.presentation;

import com.vrares.watchlist.chat.usecase.GetMessagesUseCase;
import com.vrares.watchlist.chat.usecase.SendMessageUseCase;
import com.vrares.watchlist.core.entities.social.ChatMessage;

import java.util.ArrayList;

import javax.inject.Inject;

public class ChatPresenter implements ChatContract.Presenter {

    @Inject GetMessagesUseCase getMessagesUseCase;
    @Inject SendMessageUseCase sendMessageUseCase;

    private ChatContract.View view;

    public void attach(ChatContract.View view) {
        this.view = view;
    }

    public void detach() {

    }

    public void getMessages(String chatRoomKey) {
        getMessagesUseCase.getMessages(chatRoomKey, this);
    }

    public void sendMessage(String localChatRoom, String remoteChatRoom, ChatMessage chatMessage) {
        sendMessageUseCase.sendMessage(localChatRoom, remoteChatRoom, chatMessage);
    }

    @Override
    public void onMessagesLoaded(ArrayList<ChatMessage> chatMessages) {
        view.onMessageLoaded(chatMessages);
    }

    @Override
    public void onNewMessageLoaded(ChatMessage message) {
        view.onNewMessageLoaded(message);
    }
}
