package com.vrares.watchlist.chat.presentation;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.social.ChatMessage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    public ArrayList<ChatMessage> messages;

    public ChatAdapter(ArrayList<ChatMessage> messages) {
        this.messages = messages;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ChatMessage message = messages.get(position);
        if (message.senderId.equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            holder.rootLayout.setPaddingRelative(150, 0, 0, 0);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.messageCard.getLayoutParams();
            params.removeRule(RelativeLayout.ALIGN_PARENT_START);
            params.addRule(RelativeLayout.ALIGN_PARENT_END);
            holder.messageCard.setLayoutParams(params);

            holder.messageCard.setBackgroundResource(R.drawable.local_chat_bubble);
            holder.message.setTextColor(Color.WHITE);
            holder.message.setText(message.text);
        } else {
            holder.rootLayout.setPaddingRelative(0, 0, 150, 0);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.messageCard.getLayoutParams();
            params.removeRule(RelativeLayout.ALIGN_PARENT_END);
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            holder.messageCard.setLayoutParams(params);

            holder.messageCard.setBackgroundResource(R.drawable.remote_chat_buuble);
            holder.message.setTextColor(Color.BLACK);
            holder.message.setText(message.text);
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void addMessage(ChatMessage message) {
        this.messages.add(message);
        notifyItemInserted(messages.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.message_text) TextView message;
        @BindView(R.id.message_card) CardView messageCard;
        @BindView(R.id.chat_layout_root) RelativeLayout rootLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
