package com.vrares.watchlist.core.entities.moviedetails;

public class MovieStats {

    public boolean isFavourite;
    public boolean isInWatchlist;
    public boolean isInCollection;
}
