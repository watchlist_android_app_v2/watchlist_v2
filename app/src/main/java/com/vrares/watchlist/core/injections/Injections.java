package com.vrares.watchlist.core.injections;

import android.app.Application;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.configuration.Configuration;
import toothpick.registries.FactoryRegistryLocator;
import toothpick.registries.MemberInjectorRegistryLocator;
import toothpick.smoothie.module.SmoothieApplicationModule;

public final class Injections {

    private static Scope scope;

    private Injections() {
        //Empty constructor
    }

    public static Scope getScope() {
        return scope;
    }

    public static void initInjections(Application app) {
        scope = Toothpick.openScope(app);
        scope.installModules(new SmoothieApplicationModule(app), new BaseModule());
        initConfigs();
    }

    private static void initConfigs() {
        Toothpick.setConfiguration(Configuration.forProduction().disableReflection());
        MemberInjectorRegistryLocator.setRootRegistry(new com.vrares.watchlist.MemberInjectorRegistry());
        FactoryRegistryLocator.setRootRegistry(new com.vrares.watchlist.FactoryRegistry());
    }

}
