package com.vrares.watchlist.core;

public class Constants {

    //LIST TYPES
    public static final String WATCHLIST = "watchlist";
    public static final String COLLECTION = "collection";
    public static final String FAVOURITES = "favourites";

    //FRIENDSHIP STATUS
    public static final int IS_FRIEND = 0;
    public static final int PENDING_REQUEST = 1;
    public static final int NOT_FRIENDS = 2;
    public static final int RECEIVED_FRIEND_REQUEST = 3;

    //DATABASE NODES
    public static final String FRIEND_REQUESTS = "friendRequests";
    public static final String FRIENDS = "friends";
    public static final String USERS = "users";
    public static final String SENT = "sent";
    public static final String ID = "id";
    public static final String RECEIVED = "received";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PICTURE = "picture";
    public static final String FEED = "feed";
    public static final String CHAT = "chat";
    public static final String SENDER_ID = "senderId";
    public static final String TEXT = "text";

    //NETWORKING
    public static final String IMG_URL = "http://image.tmdb.org/t/p/original";
    public static final String TMDB_URL = "https://www.themoviedb.org/movie/";
}
