package com.vrares.watchlist.core.entities.social;

import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;

public class FeedPost {

    public FeedPost() {
    }

    public String id;
    public User user;
    public MovieDetails movieDetails;
    public String timestamp;
    public String listType;

}
