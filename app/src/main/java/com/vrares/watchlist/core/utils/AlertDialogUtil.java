package com.vrares.watchlist.core.utils;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

public class AlertDialogUtil {

    private final String message;
    private final Context context;

    public AlertDialogUtil(String message, Context context) {
        this.message = message;
        this.context = context;
    }

    public void displayAlertMessage(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
}
