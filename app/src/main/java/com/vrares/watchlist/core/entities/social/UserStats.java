package com.vrares.watchlist.core.entities.social;

import com.vrares.watchlist.core.entities.User;

public class UserStats {

    public User user;
    public long collectionCount;
    public long favouritesCount;
    public long watchlistCount;
    public int friendshipStatus;
}
