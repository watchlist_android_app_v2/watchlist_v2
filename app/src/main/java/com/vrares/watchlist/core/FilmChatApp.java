package com.vrares.watchlist.core;

import android.app.Application;

import com.vrares.watchlist.core.injections.Injections;

public class FilmChatApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initInjections();
    }

    private void initInjections() {
        Injections.initInjections(this);
    }
}
