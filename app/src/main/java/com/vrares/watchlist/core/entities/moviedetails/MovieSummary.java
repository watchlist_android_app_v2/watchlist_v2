package com.vrares.watchlist.core.entities.moviedetails;

public class MovieSummary {

    public MovieStats movieStats;
    public MovieDetails movieDetails;

    public MovieSummary(MovieStats movieStats, MovieDetails movieDetails) {
        this.movieDetails = movieDetails;
        this.movieStats = movieStats;
    }
}
