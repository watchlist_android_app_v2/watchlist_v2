package com.vrares.watchlist.core.injections.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.f2prateek.dart.Dart;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import toothpick.Toothpick;

import static com.vrares.watchlist.core.injections.Injections.getScope;

public class FragmentWithInjections extends Fragment {

    private Unbinder unbinder;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        Dart.inject(this, getArguments());
        Toothpick.inject(this, getScope());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        Toothpick.closeScope(this);
    }
}
