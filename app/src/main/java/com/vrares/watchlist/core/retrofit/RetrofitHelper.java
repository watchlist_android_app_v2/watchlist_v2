package com.vrares.watchlist.core.retrofit;

import static com.vrares.watchlist.moviedb.home.ui.HomeFragment.ALL_LISTS_MODE;
import static com.vrares.watchlist.moviedb.home.ui.HomeFragment.POPULAR_MODE;
import static com.vrares.watchlist.moviedb.home.ui.HomeFragment.TOP_RATED_MODE;
import static com.vrares.watchlist.moviedb.home.ui.HomeFragment.UPCOMING_MODE;

import android.util.Log;

import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.movielist.MovieList;
import com.vrares.watchlist.core.entities.movielist.Movie;
import com.vrares.watchlist.moviedb.home.presentation.homescreen.HomeContract;
import com.vrares.watchlist.moviedb.moviedetails.presentation.MovieDetailsContract;
import com.vrares.watchlist.moviedb.searchresult.presentation.SearchResultContract;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Singleton
public class RetrofitHelper {

    private static final String BASE_URL = "https://api.themoviedb.org/";
    public static final String API_KEY_TAG = "api_key";
    private static final String API_KEY = "3923714031c68cb13bffea56cc6a9430";
    private static final String LANGUAGE_TAG = "language";
    private static final String LANGUAGE = "en-US";
    private static final String PAGE_TAG = "page";
    private static final String INCLUDE_ADULT_TAG = "include_adult";
    private static final String INCLUDE_ADULT = "true";
    private static final String SORT_BY_TAG = "sort_by";
    private static final String SORT_ASC = "created_at.asc";
    private static final String QUERY_TAG = "query";


    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void getMovieList(int pageNumber, final int listMode, final HomeContract.Presenter callback) {
        TmdbClient tmdbClient = getRetrofitInstance().create(TmdbClient.class);

        Map<String, String> data = new HashMap<>();
        data.put(API_KEY_TAG, API_KEY);
        data.put(LANGUAGE_TAG, LANGUAGE);
        data.put(PAGE_TAG, String.valueOf(pageNumber));

        Call<MovieList> call = null;

        switch (listMode) {

            case POPULAR_MODE:
                call = tmdbClient.getPopularMovieList(data);
                break;

            case TOP_RATED_MODE:
                call = tmdbClient.getTopRatedMovieList(data);
                break;

            case UPCOMING_MODE:
                call = tmdbClient.getUpcomingMovieList(data);
                break;

            case ALL_LISTS_MODE:
                getMovieList(pageNumber, POPULAR_MODE, callback);
                getMovieList(pageNumber, TOP_RATED_MODE, callback);
                getMovieList(pageNumber, UPCOMING_MODE, callback);
                break;
            default:
                Log.d("Error", "RetrofitHelper.getMovieList()");
                break;
        }

        if (call != null) {
            call.enqueue(new Callback<MovieList>() {
                @Override
                public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                    if (response.isSuccessful()) {
                        List<Movie> movies = response.body().getResults();
                        callback.onMovieListReceived(movies, listMode);
                    }
                }

                @Override
                public void onFailure(Call<MovieList> call, Throwable t) {
                    //Required override method
                }
            });
        }
    }

    public void searchForMovie(String query, final SearchResultContract.Presenter callback) {
        TmdbClient tmdbClient = getRetrofitInstance().create(TmdbClient.class);

         Map<String, String> data = new HashMap<>();
         data.put(API_KEY_TAG, API_KEY);
         data.put(QUERY_TAG, query);

         Call<MovieList> call = tmdbClient.getSearchResults(data);

         call.enqueue(new Callback<MovieList>() {
             @Override
             public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                 if (response.isSuccessful()) {
                     List<Movie> movies = response.body().getResults();
                     callback.onMovieListReceived(movies);
                 }
             }

             @Override
             public void onFailure(Call<MovieList> call, Throwable t) {

             }
         });
    }

    public void getMovieDetails(int movieId, final MovieDetailsContract.Presenter callback) {
        TmdbClient tmdbClient = getRetrofitInstance().create(TmdbClient.class);

        Map<String, String> data = new HashMap<>();
        data.put(API_KEY_TAG, API_KEY);

        Call<MovieDetails> call = tmdbClient.getMovieDetails(movieId, data);
        call.enqueue(new Callback<MovieDetails>() {
            @Override
            public void onResponse(Call<MovieDetails> call, Response<MovieDetails> response) {
                if (response.isSuccessful()) {
                    callback.onMovieDetailsReceived(response.body());
                }
            }

            @Override
            public void onFailure(Call<MovieDetails> call, Throwable t) {

            }
        });
    }
}

