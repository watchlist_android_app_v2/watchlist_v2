package com.vrares.watchlist.core.entities.movielist;

import retrofit2.Call;

public class CallEntity {

    private Call<MovieList> call;
    private int mode;

    public CallEntity(Call<MovieList> call, int mode) {
        this.call = call;
        this.mode = mode;
    }

    public Call<MovieList> getCall() {
        return call;
    }

    public void setCall(Call<MovieList> call) {
        this.call = call;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }
}
