package com.vrares.watchlist.core.retrofit;

import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.movielist.MovieList;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface TmdbClient {

    String POPULAR_URL = "/3/movie/popular";
    String TOP_RATED_URL = "/3/movie/top_rated";
    String UPCOMING_URL = "/3/movie/upcoming";
    String SEARCH_MOVIE_URL = "/3/search/movie";
    String MOVIE_DETAILS = "/3/movie/{movieId}";

    @GET(POPULAR_URL)
    Call<MovieList> getPopularMovieList(@QueryMap Map<String, String> filters);

    @GET(TOP_RATED_URL)
    Call<MovieList> getTopRatedMovieList(@QueryMap Map<String, String> filters);

    @GET(UPCOMING_URL)
    Call<MovieList> getUpcomingMovieList(@QueryMap Map<String, String> filters);

    @GET(SEARCH_MOVIE_URL)
    Call<MovieList> getSearchResults(@QueryMap Map<String, String> filters);

    @GET(MOVIE_DETAILS)
    Call<MovieDetails> getMovieDetails(@Path("movieId") int movieId, @QueryMap Map<String, String> data);
}
