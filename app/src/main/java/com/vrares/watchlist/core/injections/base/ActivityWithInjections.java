package com.vrares.watchlist.core.injections.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.f2prateek.dart.Dart;
import com.vrares.watchlist.core.injections.Injections;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import toothpick.Toothpick;

public abstract class ActivityWithInjections extends AppCompatActivity {

    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        unbinder = ButterKnife.bind(this);
        Dart.inject(this);
        Toothpick.inject(this, Injections.getScope());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        Toothpick.closeScope(this);
    }

    @LayoutRes
    public abstract int getContentView();
}
