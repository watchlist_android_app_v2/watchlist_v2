package com.vrares.watchlist.core;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.f2prateek.dart.HensonNavigable;
import com.google.firebase.auth.FirebaseAuth;
import com.vrares.watchlist.NavigationActivity;
import com.vrares.watchlist.R;
import com.vrares.watchlist.auth.WelcomeFragment;
import com.vrares.watchlist.core.injections.base.ActivityWithInjections;

@HensonNavigable
public class SplashActivity extends ActivityWithInjections {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(0, 0);

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            startActivity(new Intent(this, NavigationActivity.class));
            finish();
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new WelcomeFragment())
                .commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public int getContentView() {
        return R.layout.activity_splash;
    }
}
