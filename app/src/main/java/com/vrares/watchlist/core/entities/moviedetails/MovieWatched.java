package com.vrares.watchlist.core.entities.moviedetails;

public class MovieWatched {

    public MovieDetails movieDetails;
    public String timestamp;
    public boolean isFavourite;

    public MovieWatched(MovieDetails movieDetails, String timestamp) {
        this.movieDetails = movieDetails;
        this.timestamp = timestamp;
    }

    public MovieWatched() {
    }
}
