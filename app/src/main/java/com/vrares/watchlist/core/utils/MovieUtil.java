package com.vrares.watchlist.core.utils;

import com.vrares.watchlist.core.entities.moviedetails.Genre;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Singleton;

@Singleton
public class MovieUtil {

    public String getWatchDate(String timestamp) {
        Timestamp stamp = Timestamp.valueOf(timestamp);
        Date date = new Date(stamp.getTime());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        return dateFormat.format(date);
    }

    public String getGenres(List<Genre> genres) {
        String genresText = "";
        for (Genre genre : genres) {
            genresText = genres.indexOf(genre) < genres.size() - 1
                    ? genresText.concat(genre.getName() + ", ")
                    : genresText.concat(genre.getName());

        }
        return genresText;
    }

    public String getReleaseYear(String releaseDate) {
        return releaseDate.substring(0, releaseDate.indexOf("-"));
    }

}
