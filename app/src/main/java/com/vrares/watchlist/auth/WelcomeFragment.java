package com.vrares.watchlist.auth;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vrares.watchlist.R;
import com.vrares.watchlist.auth.login.ui.LoginFragment;
import com.vrares.watchlist.auth.register.ui.RegisterFragment;
import com.vrares.watchlist.core.injections.base.FragmentWithInjections;

import butterknife.BindAnim;
import butterknife.BindView;
import butterknife.OnClick;

public class WelcomeFragment extends FragmentWithInjections {

    @BindView(R.id.splash_logo) ImageView ivLogo;
    @BindView(R.id.splash_title) TextView tvTitle;
    @BindView(R.id.splash_subtitle) TextView tvSubtitle;
    @BindView(R.id.splash_btn_log) Button btnLogIn;
    @BindView(R.id.splash_btn_register) Button btnRegister;

    @BindAnim(R.anim.slide_from_top) Animation slideFromTop;
    @BindAnim(R.anim.fade_in) Animation fadeIn;
    @BindAnim(R.anim.fade_from_left) Animation fadeFromLeft;
    @BindAnim(R.anim.fade_from_right) Animation fadeFromRight;
    @BindAnim(R.anim.slide_to_top) Animation slideToTop;
    @BindAnim(R.anim.fade_out) Animation fadeOut;
    @BindAnim(R.anim.fade_to_left) Animation fadeToLeft;
    @BindAnim(R.anim.fade_to_right) Animation fadeToRight;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        getActivity().overridePendingTransition(0, 0);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ivLogo.setAnimation(slideFromTop);
        tvTitle.setAnimation(fadeIn);
        tvSubtitle.setAnimation(fadeIn);
        btnLogIn.setAnimation(fadeFromLeft);
        btnRegister.setAnimation(fadeFromRight);
    }

    @OnClick(R.id.splash_btn_log)
    public void goToLoginScreen() {
        deanimateWelcomeScreen(new LoginFragment());

    }

    @OnClick(R.id.splash_btn_register)
    public void goToRegisterScreen() {
        deanimateWelcomeScreen(new RegisterFragment());
    }

    private void deanimateWelcomeScreen(final Fragment fragment) {
        ivLogo.startAnimation(slideToTop);
        tvTitle.startAnimation(fadeOut);
        tvSubtitle.startAnimation(fadeOut);
        btnLogIn.startAnimation(fadeToLeft);
        btnRegister.startAnimation(fadeToRight);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //Empty on purpose
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tvSubtitle.setVisibility(View.GONE);
                transitionToSelectedFragment(fragment);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //Empty on purpose
            }
        });
    }

    protected void transitionToSelectedFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }
}
