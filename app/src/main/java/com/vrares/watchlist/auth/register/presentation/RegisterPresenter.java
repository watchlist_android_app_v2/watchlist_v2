package com.vrares.watchlist.auth.register.presentation;

import com.vrares.watchlist.auth.register.usecase.RegisterUseCase;
import com.vrares.watchlist.core.entities.User;

import javax.inject.Inject;

public class RegisterPresenter implements RegisterContract.Presenter {

    @Inject RegisterUseCase registerUseCase;

    private RegisterContract.View registerView;

    public void attach(RegisterContract.View registerView) {
        this.registerView = registerView;
    }

    public void detach() {
        this.registerView = null;
    }

    public void registerUser(User user, String password) {
        registerUseCase.registerUser(user, password, this);
    }

    @Override
    public void onRegisterFailed(Exception exception) {
        if (registerView != null) {
            registerView.onRegisterFailed(exception);
        }
    }

    @Override
    public void onUserRegistered() {
        if (registerView != null) {
            registerView.onUserRegistered();
        }
    }
}
