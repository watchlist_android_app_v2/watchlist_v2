package com.vrares.watchlist.auth.register.usecase;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.vrares.watchlist.auth.register.presentation.RegisterContract;
import com.vrares.watchlist.core.entities.User;

import javax.inject.Singleton;

@Singleton
public class RegisterUseCase {

    public static final String USERS_NODE = "users";

    private FirebaseAuth auth;

    public void registerUser(final User user, String password, final RegisterContract.Presenter registerCallback) {
        auth = FirebaseAuth.getInstance();
        auth.createUserWithEmailAndPassword(user.getEmail(), password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        doResultCallback(task, registerCallback, user);
                    }
                });
    }

    protected void doResultCallback(Task<AuthResult> task, RegisterContract.Presenter registerCallback, User user) {
        if (task.isSuccessful()) {
            insertUserIntoDatabase(user, registerCallback);
        } else {
            registerCallback.onRegisterFailed(task.getException());
        }
    }

    private void insertUserIntoDatabase(User user, final RegisterContract.Presenter registerCallback) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(USERS_NODE);
        databaseReference.child(auth.getCurrentUser().getUid()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                registerCallback.onUserRegistered();
            }
        });
    }
}
