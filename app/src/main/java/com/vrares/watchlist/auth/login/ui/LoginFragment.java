package com.vrares.watchlist.auth.login.ui;


import static com.vrares.watchlist.auth.register.ui.RegisterFragment.EMAIL_FIELD_EMPTY_ERROR;
import static com.vrares.watchlist.auth.register.ui.RegisterFragment.PASSWORD_FIELD_EMPTY_ERROR;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.vrares.watchlist.NavigationActivity;
import com.vrares.watchlist.R;
import com.vrares.watchlist.auth.login.presentation.LoginContract;
import com.vrares.watchlist.auth.login.presentation.LoginPresenter;
import com.vrares.watchlist.core.injections.base.FragmentWithInjections;
import com.vrares.watchlist.core.utils.AlertDialogUtil;

import javax.inject.Inject;


import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindAnim;
import butterknife.BindView;
import butterknife.OnClick;

public class LoginFragment extends FragmentWithInjections implements LoginContract.View {

    @BindView(R.id.login_logo) ImageView ivLogo;
    @BindView(R.id.login_email_layout) TextInputLayout tilEmail;
    @BindView(R.id.login_pass_layout) TextInputLayout tilPass;
    @BindView(R.id.login_btn) CircularProgressButton btnLogin;

    @BindAnim(R.anim.fade_from_left) Animation fadeFromLeft;

    @Inject LoginPresenter presenter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.attach(this);
        startAnimations();
    }

    @Override
    public void onStop() {
        btnLogin.dispose();
        presenter.detach();
        super.onStop();
    }

    private void startAnimations() {
        ivLogo.startAnimation(fadeFromLeft);
        tilEmail.startAnimation(fadeFromLeft);
        tilPass.startAnimation(fadeFromLeft);
        btnLogin.startAnimation(fadeFromLeft);
    }

    @OnClick(R.id.login_btn)
    public void login() {
        tilEmail.setError(null);
        tilPass.setError(null);

        if (isValidated()) {
            btnLogin.startAnimation();
            presenter.login(tilEmail.getEditText().getText().toString(),
                    tilPass.getEditText().getText().toString());
        }
    }

    @Override
    public void onLoginSuccess() {
        Intent intent = new Intent(getActivity(), NavigationActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onLoginFailed(Exception e) {
        AlertDialogUtil alertDialogUtil = new AlertDialogUtil(e.getMessage(), getActivity());
        alertDialogUtil.displayAlertMessage();
        btnLogin.revertAnimation();
    }

    private boolean isValidated() {
        if (tilEmail.getEditText().getText().toString().isEmpty()) {
            tilEmail.setError(EMAIL_FIELD_EMPTY_ERROR);
            return false;
        } else if (tilPass.getEditText().getText().toString().isEmpty()) {
            tilPass.setError(PASSWORD_FIELD_EMPTY_ERROR);
            return false;
        } else {
            return true;
        }
    }
}
