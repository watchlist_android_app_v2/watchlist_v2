package com.vrares.watchlist.auth.login.presentation;

public interface LoginContract {

    interface View {
        void onLoginSuccess();
        void onLoginFailed(Exception e);
    }

    interface Presenter {
        void onLoginSuccess();
        void onLoginFailed(Exception e);
    }

}
