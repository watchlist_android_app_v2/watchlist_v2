package com.vrares.watchlist.auth.register.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.vrares.watchlist.NavigationActivity;
import com.vrares.watchlist.R;
import com.vrares.watchlist.auth.login.ui.LoginFragment;
import com.vrares.watchlist.auth.register.presentation.RegisterContract;
import com.vrares.watchlist.auth.register.presentation.RegisterPresenter;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.injections.base.FragmentWithInjections;
import com.vrares.watchlist.core.utils.AlertDialogUtil;

import javax.inject.Inject;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindAnim;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;


public class RegisterFragment extends FragmentWithInjections implements RegisterContract.View {

    private static final String TAG = "tag";
    public static final String EMAIL_FIELD_EMPTY_ERROR = "Please insert a valid email address";
    public static final String PASSWORD_FIELD_EMPTY_ERROR = "Please insert a password";
    public static final String USER_PROFILE_ROOT = "https://robohash.org/";

    @BindView(R.id.register_logo) ImageView ivLogo;
    @BindView(R.id.register_name_layout) TextInputLayout tilName;
    @BindView(R.id.register_email_layout) TextInputLayout tilEmail;
    @BindView(R.id.register_pass_layout) TextInputLayout tilPass;
    @BindView(R.id.register_confirm_pass_layout) TextInputLayout tilConfirmPass;
    @BindView(R.id.register_btn) CircularProgressButton btnRegister;

    @BindAnim(R.anim.fade_from_right) Animation fadeFromRight;

    @BindString(R.string.please_insert_a_valid_email_address) String insertEmailError;
    @BindString(R.string.please_insert_a_name) String insertNameError;
    @BindString(R.string.please_insert_a_password) String insertPasswordError;
    @BindString(R.string.please_confirm_your_password) String confirmPasswordError;
    @BindString(R.string.passwords_dont_match) String matchingPasswordsError;

    @Inject RegisterPresenter registerPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        registerPresenter.attach(this);
        startAnimation();
    }

    private void startAnimation() {
        ivLogo.startAnimation(fadeFromRight);
        tilName.startAnimation(fadeFromRight);
        tilEmail.startAnimation(fadeFromRight);
        tilPass.startAnimation(fadeFromRight);
        tilConfirmPass.startAnimation(fadeFromRight);
        btnRegister.startAnimation(fadeFromRight);
    }

    @Override
    public void onStop() {
        btnRegister.dispose();
        registerPresenter.detach();
        super.onStop();
    }

    @OnClick(R.id.register_btn)
    public void registerUser() {
        removeTextInputErrors();
        if (isValidated()) {
            btnRegister.startAnimation();
            User user = new User(tilName.getEditText().getText().toString(),
                    tilEmail.getEditText().getText().toString(),
                    USER_PROFILE_ROOT + tilEmail.getEditText().getText().toString());
            String password = tilPass.getEditText().getText().toString();
            registerPresenter.registerUser(user, password);
        }
    }

    private void removeTextInputErrors() {
        tilName.setError(null);
        tilEmail.setError(null);
        tilPass.setError(null);
        tilConfirmPass.setError(null);
    }

    private boolean isValidated() {
        return isTextInputEmpty(tilName, insertNameError) &&
                isTextInputEmpty(tilEmail, insertEmailError) &&
                isTextInputEmpty(tilPass, insertPasswordError) &&
                isTextInputEmpty(tilConfirmPass, confirmPasswordError) &&
                doPasswordsMatch();

    }

    private boolean isTextInputEmpty(TextInputLayout textInputLayout, String error) {
        if (textInputLayout.getEditText().getText().toString().isEmpty()) {
            textInputLayout.setError(error);
            return false;
        } else {
            return true;
        }
    }

    private boolean doPasswordsMatch() {

        if (tilPass.getEditText().getText().toString().equals(tilConfirmPass.getEditText().getText().toString())) {
            return true;
        } else {
            tilConfirmPass.setError(matchingPasswordsError);
            return false;
        }
    }

    @Override
    public void onRegisterFailed(Exception exception) {
        AlertDialogUtil alertDialogUtil = new AlertDialogUtil(exception.getMessage(), getActivity());
        alertDialogUtil.displayAlertMessage();
        btnRegister.revertAnimation();
    }

    @Override
    public void onUserRegistered() {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, new LoginFragment());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
        else {
            Intent intent = new Intent(getActivity(), NavigationActivity.class);
            getActivity().startActivity(intent);
            getActivity().finish();
        }
    }
}
