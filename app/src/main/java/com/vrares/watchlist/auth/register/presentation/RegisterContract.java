package com.vrares.watchlist.auth.register.presentation;


public interface RegisterContract {

    interface View {
        void onRegisterFailed(Exception exception);
        void onUserRegistered();
    }

    interface Presenter {
        void onRegisterFailed(Exception exception);
        void onUserRegistered();
    }
}
