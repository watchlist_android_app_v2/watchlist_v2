package com.vrares.watchlist.auth.login.usecase;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.vrares.watchlist.auth.login.presentation.LoginContract;

import javax.inject.Singleton;

@Singleton
public class LoginUseCase {

    public void loginWithEmailAndPass(String email, String pass, final LoginContract.Presenter presenter) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        doResultCallback(task, presenter);
                    }
                });
    }

    private void doResultCallback(Task<AuthResult> task, LoginContract.Presenter presenter) {
        if (task.isSuccessful()) {
            presenter.onLoginSuccess();
        } else {
            presenter.onLoginFailed(task.getException());
        }
    }
}
