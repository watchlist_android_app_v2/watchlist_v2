package com.vrares.watchlist.auth.login.presentation;

import com.vrares.watchlist.auth.login.usecase.LoginUseCase;

import javax.inject.Inject;

public class LoginPresenter implements LoginContract.Presenter {

    @Inject LoginUseCase loginUseCase;

    private LoginContract.View view;

    public void attach(LoginContract.View view) {
        this.view = view;
    }

    public void detach() {
        this.view = null;
    }

    public void login(String email, String pass) {
        loginUseCase.loginWithEmailAndPass(email, pass, this);
    }

    @Override
    public void onLoginSuccess() {
        if (view != null) {
            view.onLoginSuccess();
        }
    }

    @Override
    public void onLoginFailed(Exception e) {
        view.onLoginFailed(e);
    }
}
