package com.vrares.watchlist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.f2prateek.dart.HensonNavigable;
import com.vrares.watchlist.core.injections.base.ActivityWithInjections;
import com.vrares.watchlist.moviedb.collections.ui.CollectionsFragment;
import com.vrares.watchlist.moviedb.home.ui.HomeFragment;
import com.vrares.watchlist.social.ui.SocialFragment;
import com.vrares.watchlist.moviedb.watch.ui.WatchListFragment;

import butterknife.BindView;

@HensonNavigable
public class NavigationActivity extends ActivityWithInjections implements BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.navigation) BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigationView.setOnNavigationItemSelectedListener(this);
        initHomeFragment();
    }

    private void initHomeFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container, new HomeFragment());
        fragmentTransaction.commit();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_navigation;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragmentTransaction.replace(R.id.main_container, new HomeFragment());
                fragmentTransaction.commit();
                return true;
            case R.id.navigation_collection:
                fragmentTransaction.replace(R.id.main_container, new CollectionsFragment());
                fragmentTransaction.commit();
                return true;
            case R.id.navigation_watch_list:
                fragmentTransaction.replace(R.id.main_container, new WatchListFragment());
                fragmentTransaction.commit();
                return true;
            case R.id.navigation_social:
                fragmentTransaction.replace(R.id.main_container, new SocialFragment());
                fragmentTransaction.commit();
                return true;
            default:
                return false;
        }
    }
}
