package com.vrares.watchlist.social.presentation;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vrares.watchlist.Henson;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.User;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class UsersSearchResultAdapter extends RecyclerView.Adapter<UsersSearchResultAdapter.ViewHolder> {

    private ArrayList<User> userSearchResult;
    private User user;

    public UsersSearchResultAdapter(ArrayList<User> userSearchResult, User user) {
        this.userSearchResult = userSearchResult;
        this.user = user;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_search_result, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Glide.with(holder.itemView.getContext())
                .load(userSearchResult.get(position).getPicture())
                .into(holder.profilePicture);
        holder.name.setText(userSearchResult.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(holder.itemView.getContext())
                        .gotoUserProfileActivity()
                        .localUser(user)
                        .userID(userSearchResult.get(position).getId())
                        .build();
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userSearchResult.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_picture_user_search) CircleImageView profilePicture;
        @BindView(R.id.name_user_search) TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
