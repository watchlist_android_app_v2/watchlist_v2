package com.vrares.watchlist.social.usecase.get;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.entities.social.FeedPost;
import com.vrares.watchlist.core.entities.User;

import java.util.ArrayList;

import javax.inject.Singleton;

import static com.vrares.watchlist.core.Constants.FEED;

@Singleton
public class GetFeedUseCase {

    public void getFeed(ArrayList<User> friends, final GetUserInfoCallback callback) {
        final ArrayList<String> friendIds = new ArrayList<>();
        final ArrayList<FeedPost> feedPosts = new ArrayList<>();

        for (User user : friends) {
            friendIds.add(user.getId());
        }

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(FEED);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (friendIds.contains(snapshot.getKey())) {
                        for (DataSnapshot feedSnapshot : snapshot.getChildren()) {
                            feedPosts.add(feedSnapshot.getValue(FeedPost.class));
                        }
                    }
                }

                callback.onFeedPostsReceived(feedPosts);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
