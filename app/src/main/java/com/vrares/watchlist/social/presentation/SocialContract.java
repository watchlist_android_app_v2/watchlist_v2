package com.vrares.watchlist.social.presentation;

import com.vrares.watchlist.core.entities.social.FeedPost;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.social.UserStats;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;

import java.util.ArrayList;

public interface SocialContract {

    interface View {

        void onUserInfoReceived(User user,
                                ArrayList<MovieWatched> collection,
                                ArrayList<MovieDetails> watchList,
                                ArrayList<MovieDetails> favourites,
                                ArrayList<User> friends,
                                ArrayList<User> friendRequests,
                                ArrayList<FeedPost> feedPosts);

        void onSearchResult(ArrayList<User> usersFound);

        void onFriendRequestAccepted(UserStats user);

        void onFriendRequestDeclined(UserStats user);
    }

    interface Presenter {

        void onUserInfoReceived(User user,
                                ArrayList<MovieWatched> collection,
                                ArrayList<MovieDetails> watchList,
                                ArrayList<MovieDetails> favourites,
                                ArrayList<User> friends, ArrayList<User>
                                        friendRequests,
                                ArrayList<FeedPost> feedPosts);

        void onSearchResult(ArrayList<User> usersFound);
    }

}
