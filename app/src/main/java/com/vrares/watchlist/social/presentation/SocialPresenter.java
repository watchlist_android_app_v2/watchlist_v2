package com.vrares.watchlist.social.presentation;

import com.vrares.watchlist.core.entities.social.FeedPost;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.social.UserStats;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.social.usecase.get.GetUserInfoUseCase;
import com.vrares.watchlist.social.usecase.post.CreateFriendshipUseCase;
import com.vrares.watchlist.social.usecase.post.RemoveFriendRequestUseCase;
import com.vrares.watchlist.social.usecase.post.SearchForUserUseCase;

import java.util.ArrayList;

import javax.inject.Inject;

public class SocialPresenter implements SocialContract.Presenter {

    @Inject GetUserInfoUseCase getUserInfoUseCase;
    @Inject SearchForUserUseCase searchForUserUseCase;
    @Inject CreateFriendshipUseCase createFriendshipUseCase;
    @Inject RemoveFriendRequestUseCase removeFriendRequestUseCase;

    private SocialContract.View view;

    public void attach(SocialContract.View view) {
        this.view = view;
    }

    public void detach() {
        this.view = null;
    }

    public void getUserInfo() {
        getUserInfoUseCase.getUserInfo(this);
    }

    public void searchForUsers(String query) {
        searchForUserUseCase.searchForUse(query, this);
    }

    @Override
    public void onUserInfoReceived(User user,
                                   ArrayList<MovieWatched> collection,
                                   ArrayList<MovieDetails> watchList,
                                   ArrayList<MovieDetails> favourites,
                                   ArrayList<User> friends,
                                   ArrayList<User> friendRequests,
                                   ArrayList<FeedPost> feedPosts) {

        view.onUserInfoReceived(user,
                collection,
                watchList,
                favourites,
                friends,
                friendRequests,
                feedPosts);
    }

    @Override
    public void onSearchResult(ArrayList<User> usersFound) {
        view.onSearchResult(usersFound);
    }

    public void createFriendship(User localUser, UserStats remoteUser) {
        createFriendshipUseCase.createFriendship(localUser, remoteUser);
    }

    public void removeFriendRequest(User localUser, UserStats remoteUser) {
        removeFriendRequestUseCase.removeFriendRequest(localUser, remoteUser);
    }
}
