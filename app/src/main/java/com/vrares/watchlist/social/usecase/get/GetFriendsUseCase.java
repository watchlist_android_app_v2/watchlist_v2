package com.vrares.watchlist.social.usecase.get;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.Constants;
import com.vrares.watchlist.core.entities.User;

import java.util.ArrayList;

import javax.inject.Singleton;

@Singleton
public class GetFriendsUseCase {

    public void getFriends(final GetUserInfoCallback callback) {
        DatabaseReference friendsReference = FirebaseDatabase.getInstance()
                .getReference(Constants.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constants.FRIENDS);
        final DatabaseReference friendRequestsReference = FirebaseDatabase.getInstance()
                .getReference(Constants.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constants.FRIEND_REQUESTS)
                .child(Constants.RECEIVED);

        friendsReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<User> friends = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = new User();
                    user.setId(snapshot.getKey());
                    user.setName(snapshot.child(Constants.NAME).getValue().toString());
                    user.setEmail(snapshot.child(Constants.EMAIL).getValue().toString());
                    user.setPicture(snapshot.child(Constants.PICTURE).getValue().toString());
                    friends.add(user);
                }

                getFriendRequests(friends, callback, friendRequestsReference);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getFriendRequests(final ArrayList<User> friends, final GetUserInfoCallback callback, DatabaseReference friendRequestsReference) {
        friendRequestsReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<User> friendRequests = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = new User();
                    user.setId(snapshot.getKey());
                    user.setName(snapshot.child(Constants.NAME).getValue().toString());
                    user.setEmail(snapshot.child(Constants.EMAIL).getValue().toString());
                    user.setPicture(snapshot.child(Constants.PICTURE).getValue().toString());
                    friendRequests.add(user);
                }

                callback.onFriendsReceived(friends, friendRequests);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
