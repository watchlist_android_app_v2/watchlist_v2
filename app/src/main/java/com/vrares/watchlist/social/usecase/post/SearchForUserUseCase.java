package com.vrares.watchlist.social.usecase.post;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.social.presentation.SocialContract;

import java.util.ArrayList;

import javax.inject.Singleton;

@Singleton
public class SearchForUserUseCase {

    public void searchForUse(final String query, final SocialContract.Presenter callback) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("users");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<User> usersFound = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (!snapshot.getKey().equals(FirebaseAuth.getInstance().getUid())
                            && snapshot.child("name").getValue().toString().toLowerCase().contains(query)) {

                        User user = new User();
                        user.setId(snapshot.getKey());
                        user.setName(snapshot.child("name").getValue().toString());
                        user.setEmail(snapshot.child("email").getValue().toString());
                        user.setPicture(snapshot.child("picture").getValue().toString());

                        usersFound.add(user);
                    }
                }

                callback.onSearchResult(usersFound);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
