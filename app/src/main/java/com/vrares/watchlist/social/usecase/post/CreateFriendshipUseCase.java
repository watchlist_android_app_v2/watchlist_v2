package com.vrares.watchlist.social.usecase.post;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.Constants;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.social.UserStats;

import javax.inject.Singleton;

@Singleton
public class CreateFriendshipUseCase {

    public void createFriendship(final User localUser, final UserStats remoteUser) {
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constants.USERS);

        reference.child(localUser.getId())
                .child(Constants.FRIENDS)
                .child(remoteUser.user.getId())
                .setValue(remoteUser.user);

        reference.child(remoteUser.user.getId())
                .child(Constants.FRIENDS)
                .child(localUser.getId())
                .setValue(localUser);

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(localUser.getId()).child(Constants.FRIEND_REQUESTS).child(Constants.RECEIVED).hasChild(remoteUser.user.getId())) {
                    reference.child(localUser.getId()).child(Constants.FRIEND_REQUESTS).child(Constants.RECEIVED).child(remoteUser.user.getId()).removeValue();
                } else if (dataSnapshot.child(localUser.getId()).child(Constants.FRIEND_REQUESTS).child(Constants.SENT).hasChild(remoteUser.user.getId())) {
                    reference.child(localUser.getId()).child(Constants.FRIEND_REQUESTS).child(Constants.SENT).child(remoteUser.user.getId()).removeValue();
                }

                if (dataSnapshot.child(remoteUser.user.getId()).child(Constants.FRIEND_REQUESTS).child(Constants.RECEIVED).hasChild(localUser.getId())) {
                    reference.child(remoteUser.user.getId()).child(Constants.FRIEND_REQUESTS).child(Constants.RECEIVED).child(localUser.getId()).removeValue();
                } else if (dataSnapshot.child(remoteUser.user.getId()).child(Constants.FRIEND_REQUESTS).child(Constants.SENT).hasChild(localUser.getId())) {
                    reference.child(remoteUser.user.getId()).child(Constants.FRIEND_REQUESTS).child(Constants.SENT).child(localUser.getId()).removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
