package com.vrares.watchlist.social.ui;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.vrares.watchlist.Henson;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.Constants;
import com.vrares.watchlist.core.entities.social.FeedPost;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.social.UserStats;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.core.injections.base.FragmentWithInjections;
import com.vrares.watchlist.social.presentation.FeedAdapter;
import com.vrares.watchlist.social.presentation.FriendsAdapter;
import com.vrares.watchlist.social.presentation.NoFriendsAdapter;
import com.vrares.watchlist.social.presentation.SocialContract;
import com.vrares.watchlist.social.presentation.SocialPresenter;
import com.vrares.watchlist.social.presentation.UsersSearchResultAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SocialFragment extends FragmentWithInjections implements SocialContract.View {

    public static final int SEARCH_LIST = 1;
    public static final int FRIENDS_LIST = 0;

    @Inject SocialPresenter presenter;

    @BindView(R.id.social_layout) RelativeLayout socialLayout;
    @BindView(R.id.social_content) ConstraintLayout socialContent;
    @BindView(R.id.social_loading) ProgressBar socialLoading;

    //Main layer
    @BindView(R.id.social_header) View header;
    @BindView(R.id.search_bar_social) View searchForUser;
    @BindView(R.id.friends_btn) Button btnFriends;
    @BindView(R.id.friends_activity_btn) Button friendsActivityButton;
    @BindView(R.id.friends_activity_list) RecyclerView friendsActivityList;
    @BindView(R.id.friends_list) RecyclerView friendsList;
    @BindView(R.id.user_search_list) RecyclerView userSearchList;
    @BindView(R.id.list_loading) ProgressBar listLoading;
    @BindView(R.id.back_list_btn) AppCompatButton backListBtn;
    @BindView(R.id.user_search_input) AppCompatEditText searchInput;
    @BindView(R.id.friends_list_content) ConstraintLayout friendsListContent;
    @BindView(R.id.user_search_btn) Button searchBtn;
    @BindView(R.id.empty_feed_warning) TextView emptyFeedWarning;

    //Header layer
    private CircleImageView userProfilePicture;
    private TextView userName;
    private TextView userEmail;
    private Button logoutButton;
    private View userStats;

    //Social Stats layer
    private TextView collectionStats;
    private TextView favouriteStats;
    private TextView watchlistStats;

    //Data
    private ArrayList<User> friends;
    private ArrayList<User> friendRequests;
    private ArrayList<User> userSearchResults;
    private ArrayList<FeedPost> feedPosts;
    private User localUser;

    private FriendsAdapter friendsAdapter;
    private NoFriendsAdapter noFriendsAdapter;
    private UsersSearchResultAdapter usersSearchResultAdapter;
    private FeedAdapter feedAdapter;

    public SocialFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_social, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.attach(this);
        initViews();
        presenter.getUserInfo();
    }

    @Override
    public void onStop() {
        presenter.detach();
        super.onStop();
    }

    private void initViews() {
        userProfilePicture = header.findViewById(R.id.social_header_picture);
        userName = header.findViewById(R.id.social_header_name);
        userEmail = header.findViewById(R.id.social_header_email);
        logoutButton = header.findViewById(R.id.btn_logout);
        userStats = header.findViewById(R.id.social_stats);

        collectionStats = userStats.findViewById(R.id.collection_stats);
        favouriteStats = userStats.findViewById(R.id.favourites_stats);
        watchlistStats = userStats.findViewById(R.id.watchlist_stats);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                    Intent intent = Henson.with(getActivity())
                            .gotoSplashActivity()
                            .build();
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchInput.onEditorAction(EditorInfo.IME_ACTION_DONE);
                String query = searchInput.getText().toString();
                searchInput.setText("");

                friendsList.setVisibility(View.GONE);
                listLoading.setVisibility(View.VISIBLE);
                presenter.searchForUsers(query);
            }
        });

        backListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backListBtn.setVisibility(View.GONE);
                userSearchList.setVisibility(View.GONE);
                friendsList.setVisibility(View.VISIBLE);
            }
        });

        friendsActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                friendsActivityButton.setBackgroundResource(R.drawable.round_blue_button);
                btnFriends.setBackgroundColor(Color.TRANSPARENT);
                friendsListContent.setVisibility(View.GONE);
                friendsActivityList.setVisibility(View.VISIBLE);
            }
        });

        btnFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnFriends.setBackgroundResource(R.drawable.round_blue_button);
                friendsActivityButton.setBackgroundColor(Color.TRANSPARENT);
                friendsActivityList.setVisibility(View.GONE);
                emptyFeedWarning.setVisibility(View.GONE);
                friendsListContent.setVisibility(View.VISIBLE);

            }
        });
    }

    @Override
    public void onUserInfoReceived(User user,
                                   ArrayList<MovieWatched> collection,
                                   ArrayList<MovieDetails> watchList,
                                   ArrayList<MovieDetails> favourites,
                                   ArrayList<User> friends,
                                   ArrayList<User> friendRequests,
                                   ArrayList<FeedPost> feedPosts) {
        this.localUser = user;
        socialLoading.setVisibility(View.GONE);
        socialContent.setVisibility(View.VISIBLE);
        Glide.with(getActivity())
                .load(user.getPicture())
                .into(userProfilePicture);
        userName.setText(user.getName());
        userEmail.setText(user.getEmail());

        collectionStats.setText(String.format(getString(R.string.collection_stats), String.valueOf(collection.size())));
        watchlistStats.setText(String.format(getString(R.string.watchlist_stats), String.valueOf(watchList.size())));
        favouriteStats.setText(String.format(getString(R.string.favourites_stats), String.valueOf(favourites.size())));

        this.feedPosts = feedPosts;
        this.friends = friends;
        this.friendRequests = friendRequests;

        populateFeed();
        populateFriends();
    }

    @Override
    public void onSearchResult(ArrayList<User> usersFound) {
        userSearchResults = usersFound;
        if (!usersFound.isEmpty()) {
            usersSearchResultAdapter = new UsersSearchResultAdapter(userSearchResults, localUser);
            userSearchList.setAdapter(usersSearchResultAdapter);
            userSearchList.setLayoutManager(new LinearLayoutManager(getActivity()));
            usersSearchResultAdapter.notifyDataSetChanged();
        } else {
            userSearchList.setBackgroundColor(Color.TRANSPARENT);
            noFriendsAdapter = new NoFriendsAdapter(SEARCH_LIST);
            userSearchList.setAdapter(noFriendsAdapter);
            userSearchList.setLayoutManager(new LinearLayoutManager(getActivity()));
            noFriendsAdapter.notifyDataSetChanged();
        }

        listLoading.setVisibility(View.GONE);
        backListBtn.setVisibility(View.VISIBLE);
        userSearchList.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFriendRequestAccepted(UserStats remoteUser) {
        presenter.createFriendship(localUser, remoteUser);
    }

    @Override
    public void onFriendRequestDeclined(UserStats remoteUser) {
        presenter.removeFriendRequest(localUser, remoteUser);
    }

    private void populateFeed() {
        if (feedPosts.isEmpty()) {
            friendsActivityList.setVisibility(View.GONE);
            if (friendsListContent.getVisibility() == View.VISIBLE) {
                emptyFeedWarning.setVisibility(View.GONE);
            } else {
                emptyFeedWarning.setVisibility(View.VISIBLE);
            }
        } else {

            feedAdapter = new FeedAdapter(getSortedFeed(feedPosts), localUser);
            friendsActivityList.setAdapter(feedAdapter);
            friendsActivityList.setLayoutManager(new LinearLayoutManager(getActivity()));
            feedAdapter.notifyDataSetChanged();

            if (friendsList.getVisibility() != View.VISIBLE) {
                friendsActivityList.setVisibility(View.VISIBLE);
            }
            emptyFeedWarning.setVisibility(View.GONE);
        }
    }

    private ArrayList<FeedPost> getSortedFeed(ArrayList<FeedPost> feedPosts) {
        Collections.sort(feedPosts, new Comparator<FeedPost>() {
            @Override
            public int compare(FeedPost o1, FeedPost o2) {
                if (Long.valueOf(o1.timestamp).equals(Long.valueOf(o2.timestamp))) {
                    return 0;
                }
                return Long.valueOf(o1.timestamp) > Long.valueOf(o2.timestamp) ? -1 : 1;
            }
        });
        return feedPosts;
    }

    private void populateFriends() {
        if (friends.isEmpty() && friendRequests.isEmpty()) {
            friendsList.setBackgroundColor(Color.TRANSPARENT);

            noFriendsAdapter = new NoFriendsAdapter(FRIENDS_LIST);
            friendsList.setAdapter(noFriendsAdapter);
            friendsList.setLayoutManager(new LinearLayoutManager(getActivity()));
            noFriendsAdapter.notifyDataSetChanged();
        } else {
            friendsAdapter = new FriendsAdapter(handleDisplayList(friends, friendRequests), this, localUser);
            friendsList.setAdapter(friendsAdapter);
            friendsList.setLayoutManager(new LinearLayoutManager(getActivity()));
            friendsAdapter.notifyDataSetChanged();
        }
    }

    private ArrayList<UserStats> handleDisplayList(ArrayList<User> friends, ArrayList<User> friendRequests) {
        ArrayList<UserStats> users = new ArrayList<>();
        for (User friendRequest : friendRequests) {
            UserStats user = new UserStats();
            user.user = friendRequest;
            user.friendshipStatus = Constants.RECEIVED_FRIEND_REQUEST;
            users.add(user);
        }
        for (User friend : friends) {
            UserStats user = new UserStats();
            user.user = friend;
            user.friendshipStatus = Constants.IS_FRIEND;
            users.add(user);
        }

        return users;
    }
}
