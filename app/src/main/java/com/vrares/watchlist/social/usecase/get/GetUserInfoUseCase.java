package com.vrares.watchlist.social.usecase.get;

import com.vrares.watchlist.core.entities.social.FeedPost;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;
import com.vrares.watchlist.moviedb.collections.usecase.GetCollectionUseCase;
import com.vrares.watchlist.moviedb.watch.usecase.GetWatchListUseCase;
import com.vrares.watchlist.social.presentation.SocialContract;

import java.util.ArrayList;

import javax.inject.Inject;

public class GetUserInfoUseCase implements GetUserInfoCallback {

    @Inject GetUserDetailsUseCase getUserDetailsUseCase;
    @Inject GetCollectionUseCase getCollectionUseCase;
    @Inject GetWatchListUseCase getWatchListUseCase;
    @Inject GetFriendsUseCase getFriendsUseCase;
    @Inject GetFeedUseCase getFeedUseCase;

    private SocialContract.Presenter callback;

    private User user;
    private ArrayList<MovieWatched> collection;
    private ArrayList<MovieDetails> watchList;
    private ArrayList<MovieDetails> favourites;
    private ArrayList<User> friends;
    private ArrayList<User> friendRequests;
    private ArrayList<FeedPost> feedPosts;

    public void getUserInfo(SocialContract.Presenter callback) {
        this.callback = callback;
        getUserStats();
    }

    private void getUserStats() {
        getUserDetailsUseCase.getUserDetails(this);
        getCollectionUseCase.getCollection(this);
        getWatchListUseCase.getWatchList(this);
        getFriendsUseCase.getFriends(this);
    }

    @Override
    public void onCollectionReceived(ArrayList<MovieWatched> collection) {
        this.collection = collection;
        trySendingCallback();
    }

    @Override
    public void onWatchListAndFavouritesReceived(ArrayList<MovieDetails> watchList, ArrayList<MovieDetails> favourites) {
        this.watchList = watchList;
        this.favourites = favourites;
        trySendingCallback();
    }

    @Override
    public void onUserDetailsReceived(User user) {
        this.user = user;
        trySendingCallback();
    }

    @Override
    public void onFriendsReceived(ArrayList<User> friends, ArrayList<User> friendRequests) {
        this.friends = friends;
        this.friendRequests = friendRequests;
        getFeedUseCase.getFeed(friends, this);
        trySendingCallback();
    }

    @Override
    public void onFeedPostsReceived(ArrayList<FeedPost> feedPosts) {
        this.feedPosts = feedPosts;
        trySendingCallback();
    }

    private void trySendingCallback() {
        if (user != null
                && collection != null
                && watchList != null
                && favourites != null
                && friends != null
                && friendRequests != null
                && feedPosts != null) {
            callback.onUserInfoReceived(user,
                    collection,
                    watchList,
                    favourites,
                    friends,
                    friendRequests,
                    feedPosts);
        }
    }
}
