package com.vrares.watchlist.social.presentation;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vrares.watchlist.Henson;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.Constants;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.social.UserStats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.FriendsViewHolder> {

    private ArrayList<UserStats> users;
    private SocialContract.View view;
    private User localUser;

    public FriendsAdapter(ArrayList<UserStats> users, SocialContract.View view, User localUser) {
        this.users = users;
        this.view = view;
        this.localUser = localUser;
    }

    @Override
    public FriendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friends, parent, false);
        return new FriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FriendsViewHolder holder, final int position) {
        final UserStats user = users.get(position);
        if (user.friendshipStatus == Constants.RECEIVED_FRIEND_REQUEST) {
            holder.acceptRequest.setVisibility(View.VISIBLE);
            holder.declineRequest.setVisibility(View.VISIBLE);
            holder.chatBtn.setVisibility(View.GONE);
        } else {
            holder.acceptRequest.setVisibility(View.GONE);
            holder.declineRequest.setVisibility(View.GONE);
            holder.chatBtn.setVisibility(View.VISIBLE);
        }

        Glide.with(holder.itemView.getContext())
                .load(user.user.getPicture())
                .into(holder.profilePicture);
        holder.name.setText(user.user.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(holder.itemView.getContext())
                        .gotoUserProfileActivity()
                        .localUser(localUser)
                        .userID(user.user.getId())
                        .build();
                holder.itemView.getContext().startActivity(intent);
            }
        });

        holder.declineRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                users.remove(user);
                notifyDataSetChanged();
                view.onFriendRequestDeclined(user);
            }
        });

        holder.acceptRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.friendshipStatus = Constants.IS_FRIEND;
                sortList();
                notifyDataSetChanged();
                view.onFriendRequestAccepted(user);
            }
        });

        holder.chatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(holder.itemView.getContext())
                        .gotoChatActivity()
                        .userId(user.user.getId())
                        .userName(user.user.getName())
                        .userPicture(user.user.getPicture())
                        .build();
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    private void sortList() {
        Collections.sort(users, new Comparator<UserStats>() {
            @Override
            public int compare(UserStats o1, UserStats o2) {
                if (o1.friendshipStatus == o2.friendshipStatus) {
                    return 0;
                }
                return o1.friendshipStatus > o2.friendshipStatus ? -1 : 1;
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class FriendsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_picture_friend_list) CircleImageView profilePicture;
        @BindView(R.id.name) TextView name;
        @BindView(R.id.chat_btn) Button chatBtn;
        @BindView(R.id.accept_btn) Button acceptRequest;
        @BindView(R.id.decline_btn) Button declineRequest;

        public FriendsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
