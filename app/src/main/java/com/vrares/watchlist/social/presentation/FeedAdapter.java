package com.vrares.watchlist.social.presentation;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vrares.watchlist.Henson;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.entities.social.FeedPost;
import com.vrares.watchlist.core.entities.User;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {

    private ArrayList<FeedPost> feedPosts;
    private User localUser;

    public FeedAdapter(ArrayList<FeedPost> feedPosts, User user) {
        this.feedPosts = feedPosts;
        this.localUser = user;
    }

    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friends_activity, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FeedAdapter.ViewHolder holder, int position) {
        final FeedPost feedPost = feedPosts.get(position);
        Glide.with(holder.itemView.getContext())
                .load(feedPost.user.getPicture())
                .into(holder.profilePicture);

        holder.timestamp.setText(getTimestamp(feedPost.timestamp));

        SpannableString spannableString = new SpannableString(String.format(holder.itemView.getContext()
                                                                    .getString(R.string.feed_summary),
                feedPost.user.getName(),
                feedPost.movieDetails.getTitle(),
                feedPost.listType));

        spannableString.setSpan(new ForegroundColorSpan(holder.itemView.getContext().getColor(R.color.bluePrimary)),
                0,
                feedPost.user.getName().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ForegroundColorSpan(holder.itemView.getContext().getColor(R.color.bluePrimary)),
                feedPost.user.getName().length() + 6,
                feedPost.user.getName().length() + 7 + feedPost.movieDetails.getTitle().length() + 1,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        ClickableSpan userClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = Henson.with(holder.itemView.getContext())
                        .gotoUserProfileActivity()
                        .localUser(localUser)
                        .userID(feedPost.user.getId())
                        .build();
                holder.itemView.getContext().startActivity(intent);
            }
        };

        ClickableSpan movieClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = Henson.with(holder.itemView.getContext())
                        .gotoMovieDetailsActivity()
                        .movieId(feedPost.movieDetails.getId())
                        .build();
                holder.itemView.getContext().startActivity(intent);
            }
        };

        spannableString.setSpan(userClickableSpan,
                0,
                feedPost.user.getName().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(movieClickableSpan,
                feedPost.user.getName().length() + 6,
                feedPost.user.getName().length() + 7 + feedPost.movieDetails.getTitle().length() + 1,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        holder.feedSummary.setText(spannableString);
        holder.feedSummary.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private String getTimestamp(String timestamp) {
        long longTimeStamp = Long.parseLong(timestamp);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM HH:mm");
        Date date = new Date(longTimeStamp);
        return dateFormat.format(date);
    }

    @Override
    public int getItemCount() {
        return feedPosts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_picture) CircleImageView profilePicture;
        @BindView(R.id.timestamp) TextView timestamp;
        @BindView(R.id.feed_summary) TextView feedSummary;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
