package com.vrares.watchlist.social.usecase.get;

import com.vrares.watchlist.core.entities.social.FeedPost;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.moviedetails.MovieDetails;
import com.vrares.watchlist.core.entities.moviedetails.MovieWatched;

import java.util.ArrayList;

public interface GetUserInfoCallback {
    void onCollectionReceived(ArrayList<MovieWatched> movieCollection);

    void onWatchListAndFavouritesReceived(ArrayList<MovieDetails> watchList, ArrayList<MovieDetails> favourites);

    void onUserDetailsReceived(User user);

    void onFriendsReceived(ArrayList<User> friends, ArrayList<User> friendRequests);

    void onFeedPostsReceived(ArrayList<FeedPost> feedPosts);
}
