package com.vrares.watchlist.social.presentation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vrares.watchlist.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.vrares.watchlist.social.ui.SocialFragment.SEARCH_LIST;

public class NoFriendsAdapter extends RecyclerView.Adapter<NoFriendsAdapter.FriendsViewHolder> {

    private int mode;

    public NoFriendsAdapter(int mode) {
        this.mode = mode;
    }

    @Override
    public NoFriendsAdapter.FriendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_friends, parent, false);
        return new NoFriendsAdapter.FriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NoFriendsAdapter.FriendsViewHolder holder, int position) {
        if (mode == SEARCH_LIST ) {
            holder.warning.setText(R.string.no_users_found_search);
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class FriendsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.empty_list_warning) TextView warning;

        public FriendsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
