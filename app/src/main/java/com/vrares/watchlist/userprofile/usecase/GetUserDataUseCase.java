package com.vrares.watchlist.userprofile.usecase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.Constants;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.social.UserStats;
import com.vrares.watchlist.userprofile.presentation.UserProfileContract;

import javax.inject.Singleton;

import static com.vrares.watchlist.core.Constants.COLLECTION;
import static com.vrares.watchlist.core.Constants.FAVOURITES;
import static com.vrares.watchlist.core.Constants.SENT;
import static com.vrares.watchlist.core.Constants.WATCHLIST;

@Singleton
public class GetUserDataUseCase {

    public void getUserData(final String userID, final UserProfileContract.Presenter callback) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constants.USERS);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserStats userStats = new UserStats();

                if (dataSnapshot.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constants.FRIENDS).hasChild(userID)) {
                    userStats.friendshipStatus = Constants.IS_FRIEND;
                } else if (dataSnapshot.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constants.FRIEND_REQUESTS).child(SENT).hasChild(userID)) {
                    userStats.friendshipStatus = Constants.PENDING_REQUEST;
                } else if (dataSnapshot.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constants.FRIEND_REQUESTS).child(Constants.RECEIVED).hasChild(userID)) {
                    userStats.friendshipStatus = Constants.RECEIVED_FRIEND_REQUEST;
                } else {
                    userStats.friendshipStatus = Constants.NOT_FRIENDS;
                }

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (snapshot.getKey().equals(userID)) {
                        User user = new User();
                        user.setId(snapshot.getKey());
                        user.setName(snapshot.child("name").getValue().toString());
                        user.setEmail(snapshot.child("email").getValue().toString());
                        user.setPicture(snapshot.child("picture").getValue().toString());

                        userStats.user = user;
                        userStats.collectionCount = snapshot.child(COLLECTION).getChildrenCount();
                        userStats.favouritesCount = snapshot.child(FAVOURITES).getChildrenCount();
                        userStats.watchlistCount = snapshot.child(WATCHLIST).getChildrenCount();

                        callback.onUserDataReceived(userStats);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
