package com.vrares.watchlist.userprofile.presentation;

import com.vrares.watchlist.core.entities.social.UserStats;

public interface UserProfileContract {

    interface View {

        void onUserDataReceived(UserStats userStats);
    }

    interface Presenter {

        void onUserDataReceived(UserStats userStats);
    }

}
