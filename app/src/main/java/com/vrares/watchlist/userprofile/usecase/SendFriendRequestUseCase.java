package com.vrares.watchlist.userprofile.usecase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrares.watchlist.core.Constants;
import com.vrares.watchlist.core.entities.User;

import javax.inject.Singleton;

@Singleton
public class SendFriendRequestUseCase {

    public void sendFriendRequest(final String localId, final User remoteUser) {
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constants.USERS);

        reference.child(localId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User localUser = new User();
                localUser.setPicture(dataSnapshot.child(Constants.PICTURE).getValue().toString());
                localUser.setEmail(dataSnapshot.child(Constants.EMAIL).getValue().toString());
                localUser.setName(dataSnapshot.child(Constants.NAME).getValue().toString());
                localUser.setId(localId);

                reference.child(localId)
                        .child(Constants.FRIEND_REQUESTS)
                        .child(Constants.SENT)
                        .child(remoteUser.getId())
                        .setValue(remoteUser);

                reference.child(remoteUser.getId())
                        .child(Constants.FRIEND_REQUESTS)
                        .child(Constants.RECEIVED)
                        .child(localId)
                        .setValue(localUser);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
