package com.vrares.watchlist.userprofile.presentation;

import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.social.UserStats;
import com.vrares.watchlist.social.usecase.post.CreateFriendshipUseCase;
import com.vrares.watchlist.social.usecase.post.RemoveFriendRequestUseCase;
import com.vrares.watchlist.userprofile.usecase.GetUserDataUseCase;
import com.vrares.watchlist.userprofile.usecase.SendFriendRequestUseCase;

import javax.inject.Inject;

public class UserProfilePresenter implements UserProfileContract.Presenter {

    @Inject GetUserDataUseCase getUserDataUseCase;
    @Inject SendFriendRequestUseCase sendFriendRequestUseCase;
    @Inject CreateFriendshipUseCase createFriendshipUseCase;
    @Inject RemoveFriendRequestUseCase removeFriendRequestUseCase;

    private UserProfileContract.View view;

    public void attach(UserProfileContract.View view) {
        this.view = view;
    }

    public void detach() {
        this.view = null;
    }

    public void getUserInfo(String userID) {
        getUserDataUseCase.getUserData(userID, this);
    }

    public void sendFriendRequest(String localId, User remoteUser) {
        sendFriendRequestUseCase.sendFriendRequest(localId, remoteUser);
    }

    @Override
    public void onUserDataReceived(UserStats userStats) {
        view.onUserDataReceived(userStats);
    }

    public void acceptFriendRequest(User localUser, UserStats remoteUser) {
        createFriendshipUseCase.createFriendship(localUser, remoteUser);
    }

    public void removeFriendRequest(User localUser, UserStats remoteUser) {
        removeFriendRequestUseCase.removeFriendRequest(localUser, remoteUser);
    }
}
