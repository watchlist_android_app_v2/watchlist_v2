package com.vrares.watchlist.userprofile.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.f2prateek.dart.InjectExtra;
import com.google.firebase.auth.FirebaseAuth;
import com.vrares.watchlist.Henson;
import com.vrares.watchlist.R;
import com.vrares.watchlist.core.Constants;
import com.vrares.watchlist.core.entities.User;
import com.vrares.watchlist.core.entities.social.UserStats;
import com.vrares.watchlist.core.injections.base.ActivityWithInjections;
import com.vrares.watchlist.userprofile.presentation.UserProfileContract;
import com.vrares.watchlist.userprofile.presentation.UserProfilePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.vrares.watchlist.core.Constants.COLLECTION;
import static com.vrares.watchlist.core.Constants.FAVOURITES;
import static com.vrares.watchlist.core.Constants.WATCHLIST;

public class UserProfileActivity extends ActivityWithInjections implements UserProfileContract.View {

    @Inject UserProfilePresenter presenter;
    @InjectExtra String userID;
    @InjectExtra User localUser;

    @BindView(R.id.user_profile_loading) ProgressBar loading;
    @BindView(R.id.user_profile_content) RelativeLayout content;
    @BindView(R.id.user_profile_header) View header;
    @BindView(R.id.add_friends_btn) AppCompatButton addFriendsBtn;
    @BindView(R.id.pending_friendship_status) TextView pendingFriendshipText;
    @BindView(R.id.chat_btn_profile) AppCompatButton chatBtn;
    @BindView(R.id.friend_request_choices) LinearLayout friendRequestChoices;
    @BindView(R.id.accept_friend_request_btn_profile) Button acceptFriendRequest;
    @BindView(R.id.decline_friend_request_btn_profile) Button declineFriendRequest;

    //Header layer
    private CircleImageView userProfilePicture;
    private TextView userName;
    private TextView userEmail;
    private Button actionButton;
    private View userViewCounts;

    //Social Stats layer
    private TextView collectionStats;
    private TextView favouriteStats;
    private TextView watchlistStats;

    private UserStats userStats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
    }

    private void initViews() {
        userProfilePicture = header.findViewById(R.id.social_header_picture);
        userName = header.findViewById(R.id.social_header_name);
        userEmail = header.findViewById(R.id.social_header_email);
        actionButton = header.findViewById(R.id.btn_logout);
        userViewCounts = header.findViewById(R.id.social_stats);

        collectionStats = userViewCounts.findViewById(R.id.collection_stats);
        favouriteStats = userViewCounts.findViewById(R.id.favourites_stats);
        watchlistStats = userViewCounts.findViewById(R.id.watchlist_stats);

        actionButton.setVisibility(View.GONE);

        addFriendsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UserProfileActivity.this, "Friend request sent", Toast.LENGTH_SHORT).show();
                addFriendsBtn.setVisibility(View.GONE);
                pendingFriendshipText.setVisibility(View.VISIBLE);
                presenter.sendFriendRequest(FirebaseAuth.getInstance().getCurrentUser().getUid(), userStats.user);
            }
        });

        acceptFriendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UserProfileActivity.this, "Friend Request accepted", Toast.LENGTH_SHORT).show();
                acceptFriendRequest.setVisibility(View.GONE);
                addFriendsBtn.setVisibility(View.GONE);
                declineFriendRequest.setVisibility(View.GONE);
                chatBtn.setVisibility(View.VISIBLE);
                presenter.acceptFriendRequest(localUser, userStats);
            }
        });

        declineFriendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UserProfileActivity.this, "Friend Request declined", Toast.LENGTH_SHORT).show();
                acceptFriendRequest.setVisibility(View.GONE);
                addFriendsBtn.setVisibility(View.VISIBLE);
                declineFriendRequest.setVisibility(View.GONE);
                chatBtn.setVisibility(View.GONE);
                presenter.removeFriendRequest(localUser, userStats);
            }
        });

        chatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(UserProfileActivity.this)
                        .gotoChatActivity()
                        .userId(userStats.user.getId())
                        .userName(userStats.user.getName())
                        .userPicture(userStats.user.getPicture())
                        .build();
                startActivity(intent);
            }
        });

        collectionStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(UserProfileActivity.this)
                        .gotoListActivity()
                        .listSize(String.valueOf(userStats.collectionCount))
                        .listType(COLLECTION)
                        .userId(userID)
                        .userName(userStats.user.getName())
                        .build();
                startActivity(intent);
            }
        });

        watchlistStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(UserProfileActivity.this)
                        .gotoListActivity()
                        .listSize(String.valueOf(userStats.watchlistCount))
                        .listType(WATCHLIST)
                        .userId(userID)
                        .userName(userStats.user.getName())
                        .build();
                startActivity(intent);
            }
        });

        favouriteStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Henson.with(UserProfileActivity.this)
                        .gotoListActivity()
                        .listSize(String.valueOf(userStats.favouritesCount))
                        .listType(FAVOURITES)
                        .userId(userID)
                        .userName(userStats.user.getName())
                        .build();
                startActivity(intent);
            }
        });
    }

    private void populateViews() {
        Glide.with(this)
                .load(userStats.user.getPicture())
                .into(userProfilePicture);
        userName.setText(userStats.user.getName());
        userEmail.setText(userStats.user.getEmail());
        collectionStats.setText(String.format(getString(R.string.collection_stats), String.valueOf(userStats.collectionCount)));
        favouriteStats.setText(String.format(getString(R.string.favourites_stats), String.valueOf(userStats.favouritesCount)));
        watchlistStats.setText(String.format(getString(R.string.watchlist_stats), String.valueOf(userStats.watchlistCount)));

        switch (userStats.friendshipStatus) {
            case Constants.IS_FRIEND:
                friendRequestChoices.setVisibility(View.GONE);
                addFriendsBtn.setVisibility(View.GONE);
                pendingFriendshipText.setVisibility(View.GONE);
                chatBtn.setVisibility(View.VISIBLE);
                break;
            case Constants.PENDING_REQUEST:
                friendRequestChoices.setVisibility(View.GONE);
                addFriendsBtn.setVisibility(View.GONE);
                pendingFriendshipText.setVisibility(View.VISIBLE);
                chatBtn.setVisibility(View.GONE);
                break;
            case Constants.NOT_FRIENDS:
                friendRequestChoices.setVisibility(View.GONE);
                addFriendsBtn.setVisibility(View.VISIBLE);
                pendingFriendshipText.setVisibility(View.GONE);
                chatBtn.setVisibility(View.GONE);
                break;
            case Constants.RECEIVED_FRIEND_REQUEST:
                friendRequestChoices.setVisibility(View.VISIBLE);
                addFriendsBtn.setVisibility(View.GONE);
                pendingFriendshipText.setVisibility(View.GONE);
                chatBtn.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attach(this);
        presenter.getUserInfo(userID);
    }

    @Override
    protected void onStop() {
        presenter.detach();
        super.onStop();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_user_profile;
    }

    @Override
    public void onUserDataReceived(UserStats userStats) {
        this.userStats = userStats;
        populateViews();
        loading.setVisibility(View.GONE);
        content.setVisibility(View.VISIBLE);
        header.setVisibility(View.VISIBLE);

    }
}
